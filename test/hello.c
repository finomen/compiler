void printf();
void * malloc(int a);
void free(void * a);

 int globalVariable = 2;
 
int foo() {
	++globalVariable;
}

int strlen(const char * str) {
	int len = 0;
	while (str[len++] != 0);
	return len;
}

void strcpy(char * dest, const char * src) {
	int i = 0;
	while (src[i] != 0) {
		printf("Copy char %c\n", src[i]);
		dest[i] = src[i];
		++i;
	}
	
	dest[i] = 0;
}
 
int main() {
	printf("Running tests...\n");
	
	int localVariable = 1;
	++localVariable;
	
	if (localVariable == 2) {
		printf("Local change ok\n");
	} else {
		printf("Local change fail, actual value: %d\n", localVariable);
	}
	
	printf("Local value: %d\n", localVariable);
	
	foo();
	if (globalVariable == 3) {
		printf("Global change ok\n");
	} else {
		printf("Global change fail, actual value: %d\n", globalVariable);
	}
	
	printf("Global value: %d\n", globalVariable);
	
	for (int i = 0; i < 4; ++i) {
		printf("Loop %d\n", i);
	}
	
	char * str = "Hello";
	int i = 0;
	i = 0;
	printf("Iterate over %s\n", str);
	while (str[i] != 0 && i < 100) {
		printf("Char at %d = %c @ %d\n", i, str[i], str + i);
		++i;
	}
	
	printf("Copy string of length %d\n", strlen(str));
	
	char * copy = malloc(strlen(str));
	printf("Allocated buffer at address %d\n", copy);
	strcpy(copy, str);
	printf("String copy %s\n", copy);
	free(str);
	
	
	
	return 0;
 }