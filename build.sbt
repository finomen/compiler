name := "Compiler"

version := "1.0"

scalaVersion := "2.11.0-RC3"


libraryDependencies += "org.scala-lang.modules" %% "scala-parser-combinators" % "1.0.1"

libraryDependencies += "org.scalatest" % "scalatest_2.11.0-RC3" % "2.1.2" % "test"
