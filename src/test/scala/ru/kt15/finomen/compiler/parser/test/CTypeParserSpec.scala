package ru.kt15.finomen.compiler.parser.test

/**
 * Created by finomen on 02.04.14.
 */
import collection.mutable.Stack
import org.scalatest.{Matchers, FlatSpec}
import ru.kt15.finomen.compiler.ast.types.builtin._
import ru.kt15.finomen.compiler.ast.types.{AliasType, ConstType, ArrayType}
import ru.kt15.finomen.compiler.ast.types.pointers.{VoidPointer, PointerType}
import ru.kt15.finomen.compiler.env.Context

object CTypeParser extends AnyRef with ru.kt15.finomen.compiler.parser.types.CTypeParser {
}

class CTypeParserSpec extends FlatSpec with Matchers {

  def context = new Context()

  "BuiltInTypeParser" should "parse int" in {
    val res = CTypeParser.parseAll(CTypeParser.cTypeParser(context), "int").get
    res shouldBe a [Int]
    res.asInstanceOf[Int].signModifier should be (SignModifier.Signed)
    res.asInstanceOf[Int].sizeModifier should be (SizeModifier.None)
  }

  "BuiltInTypeParser" should "parse signed int" in {
    val res = CTypeParser.parseAll(CTypeParser.cTypeParser(context), "signed int").get
    res shouldBe a [Int]
    res.asInstanceOf[Int].signModifier should be (SignModifier.Signed)
    res.asInstanceOf[Int].sizeModifier should be (SizeModifier.None)
  }

  "BuiltInTypeParser" should "parse unsigned int" in {
    val res = CTypeParser.parseAll(CTypeParser.cTypeParser(context), "unsigned int").get
    res shouldBe a [Int]
    res.asInstanceOf[Int].signModifier should be (SignModifier.Unsigned)
    res.asInstanceOf[Int].sizeModifier should be (SizeModifier.None)
  }

  "BuiltInTypeParser" should "parse long int" in {
    val res = CTypeParser.parseAll(CTypeParser.cTypeParser(context), "long int").get
    res shouldBe a [Int]
    res.asInstanceOf[Int].signModifier should be (SignModifier.Signed)
    res.asInstanceOf[Int].sizeModifier should be (SizeModifier.Long)
  }

  "BuiltInTypeParser" should "parse short int" in {
    val res = CTypeParser.parseAll(CTypeParser.cTypeParser(context), "short int").get
    res shouldBe a [Int]
    res.asInstanceOf[Int].signModifier should be (SignModifier.Signed)
    res.asInstanceOf[Int].sizeModifier should be (SizeModifier.Short)
  }

  "BuiltInTypeParser" should "parse unsigned short int" in {
    val res = CTypeParser.parseAll(CTypeParser.cTypeParser(context), "unsigned short int").get
    res shouldBe a [Int]
    res.asInstanceOf[Int].signModifier should be (SignModifier.Unsigned)
    res.asInstanceOf[Int].sizeModifier should be (SizeModifier.Short)
  }

  "BuiltInTypeParser" should "parse unsigned long int" in {
    val res = CTypeParser.parseAll(CTypeParser.cTypeParser(context), "unsigned long int").get
    res shouldBe a [Int]
    res.asInstanceOf[Int].signModifier should be (SignModifier.Unsigned)
    res.asInstanceOf[Int].sizeModifier should be (SizeModifier.Long)
  }

  "BuiltInTypeParser" should "parse short" in {
    val res = CTypeParser.parseAll(CTypeParser.cTypeParser(context), "short").get
    res shouldBe a [Int]
    res.asInstanceOf[Int].signModifier should be (SignModifier.Signed)
    res.asInstanceOf[Int].sizeModifier should be (SizeModifier.Short)
  }

  "BuiltInTypeParser" should "parse long" in {
    val res = CTypeParser.parseAll(CTypeParser.cTypeParser(context), "long").get
    res shouldBe a [Int]
    res.asInstanceOf[Int].signModifier should be (SignModifier.Signed)
    res.asInstanceOf[Int].sizeModifier should be (SizeModifier.Long)
  }

  "BuiltInTypeParser" should "parse unsigned short" in {
    val res = CTypeParser.parseAll(CTypeParser.cTypeParser(context), "unsigned short").get
    res shouldBe a [Int]
    res.asInstanceOf[Int].signModifier should be (SignModifier.Unsigned)
    res.asInstanceOf[Int].sizeModifier should be (SizeModifier.Short)
  }

  "BuiltInTypeParser" should "parse unsigned long" in {
    val res = CTypeParser.parseAll(CTypeParser.cTypeParser(context), "unsigned long").get
    res shouldBe a [Int]
    res.asInstanceOf[Int].signModifier should be (SignModifier.Unsigned)
    res.asInstanceOf[Int].sizeModifier should be (SizeModifier.Long)
  }

  "BuiltInTypeParser" should "parse char" in {
    val res = CTypeParser.parseAll(CTypeParser.cTypeParser(context), "char").get
    res shouldBe a [Char]
    res.asInstanceOf[Char].signModifier should be (SignModifier.Signed)
  }

  "BuiltInTypeParser" should "parse signed char" in {
    val res = CTypeParser.parseAll(CTypeParser.cTypeParser(context), "signed char").get
    res shouldBe a [Char]
    res.asInstanceOf[Char].signModifier should be (SignModifier.Signed)
  }

  "BuiltInTypeParser" should "parse unsigned char" in {
    val res = CTypeParser.parseAll(CTypeParser.cTypeParser(context), "unsigned char").get
    res shouldBe a [Char]
    res.asInstanceOf[Char].signModifier should be (SignModifier.Unsigned)
  }

  "BuiltInTypeParser" should "parse float" in {
    val res = CTypeParser.parseAll(CTypeParser.cTypeParser(context), "float").get
    res shouldBe a [Float]
  }

  "BuiltInTypeParser" should "parse double" in {
    val res = CTypeParser.parseAll(CTypeParser.cTypeParser(context), "double").get
    res shouldBe a [Double]
    res.asInstanceOf[Double].sizeModifier should be (SizeModifier.None)
  }

  "BuiltInTypeParser" should "parse long double" in {
    val res = CTypeParser.parseAll(CTypeParser.cTypeParser(context), "long double").get
    res shouldBe a [Double]
    res.asInstanceOf[Double].sizeModifier should be (SizeModifier.Long)
  }

  "ArrayTypeParser" should "parse long double[]" in {
    val res = CTypeParser.parseAll(CTypeParser.cTypeParser(context), "long double[]").get
    res shouldBe a [ArrayType]
    res.asInstanceOf[ArrayType].innerType shouldBe a [Double]
    val inner = res.asInstanceOf[ArrayType].innerType
    inner.asInstanceOf[Double].sizeModifier should be (SizeModifier.Long)
  }

  "ConstTypeParser" should "parse const int" in {
    val res = CTypeParser.parseAll(CTypeParser.cTypeParser(context), "const int").get
    res shouldBe a [ConstType]
    res.asInstanceOf[ConstType].innerType shouldBe a [Int]
    val inner = res.asInstanceOf[ConstType].innerType
    inner.asInstanceOf[Int].sizeModifier should be (SizeModifier.None)
    inner.asInstanceOf[Int].signModifier should be (SignModifier.Signed)
  }

  "AliasTypeParser" should "parse userDefined" in {
    val c = context
    c += ("userDefined" -> new Int(SignModifier.Signed, SizeModifier.None))
    val res = CTypeParser.parseAll(CTypeParser.cTypeParser(c), "userDefined").get

    res shouldBe a [AliasType]
    val inner = res.asInstanceOf[AliasType].flatten
    inner.asInstanceOf[Int].signModifier should be (SignModifier.Signed)
    inner.asInstanceOf[Int].sizeModifier should be (SizeModifier.None)
  }

  "PointerTypeParser" should "parse const int *" in {
    val pres = CTypeParser.parseAll(CTypeParser.cTypeParser(context), "const int *").get
    pres shouldBe a [PointerType]
    val res = pres.asInstanceOf[PointerType].objectType
    res shouldBe a [ConstType]
    res.asInstanceOf[ConstType].innerType shouldBe a [Int]
    val inner = res.asInstanceOf[ConstType].innerType
    inner.asInstanceOf[Int].sizeModifier should be (SizeModifier.None)
    inner.asInstanceOf[Int].signModifier should be (SignModifier.Signed)
  }

  "PointerTypeParser" should "parse void *" in {
    val res = CTypeParser.parseAll(CTypeParser.cTypeParser(context), "void *").get
    res shouldBe a [VoidPointer]
  }
}
