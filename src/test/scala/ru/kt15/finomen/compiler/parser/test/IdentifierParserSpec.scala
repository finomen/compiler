package ru.kt15.finomen.compiler.parser.test

import org.scalatest.{Matchers, FlatSpec}

object IdentifierParser extends AnyRef with ru.kt15.finomen.compiler.parser.IdentifierParser {

}

/**
 * Created by finomen on 07.04.14.
 */
class IdentifierParserSpec  extends FlatSpec with Matchers {
  "IdentifierParser" should "parse someName" in {
    val res = IdentifierParser.parseAll(IdentifierParser.identifierParser, "someName").get
    res shouldBe a [String]
    res.asInstanceOf[String] shouldEqual "someName"
  }

  "IdentifierParser" should "parse _someName" in {
    val res = IdentifierParser.parseAll(IdentifierParser.identifierParser, "_someName").get
    res shouldBe a [String]
    res.asInstanceOf[String] shouldEqual "_someName"
  }

  "IdentifierParser" should "parse _1" in {
    val res = IdentifierParser.parseAll(IdentifierParser.identifierParser, "_1").get
    res shouldBe a [String]
    res.asInstanceOf[String] shouldEqual "_1"
  }

  "IdentifierParser" should "parse _" in {
    val res = IdentifierParser.parseAll(IdentifierParser.identifierParser, "_").get
    res shouldBe a [String]
    res.asInstanceOf[String] shouldEqual "_"
  }

  "IdentifierParser" should "fail 1" in {
    val res = IdentifierParser.parseAll(IdentifierParser.identifierParser, "1")
    res shouldBe a [IdentifierParser.NoSuccess]
  }

  "IdentifierParser" should "fail keywords" in {
    IdentifierParser.keywords foreach { x =>
      val res = IdentifierParser.parseAll(IdentifierParser.identifierParser, x)
      res shouldBe a [IdentifierParser.NoSuccess]
    }
  }

  "IdentifierParser" should "fail empty" in {
    val res = IdentifierParser.parseAll(IdentifierParser.identifierParser, "")
    res shouldBe a [IdentifierParser.NoSuccess]
  }
}
