package ru.kt15.finomen.compiler.parser.test

import org.scalatest.{Matchers, FlatSpec}
import ru.kt15.finomen.compiler.ast.types.builtin.{SignModifier, Char, Int}
import ru.kt15.finomen.compiler.ast.expressions._
import ru.kt15.finomen.compiler.ast.expressions.operators.{OperatorType, Operator}
import ru.kt15.finomen.compiler.parser.nodes.VariableDeclarationNodeParser
import ru.kt15.finomen.compiler.ast.nodes.VariableDeclarationNode
import ru.kt15.finomen.compiler.env.{Variable, Context}


/**
 * Created by finomen on 08.04.14.
 */
object ExpressionsParser extends AnyRef with ru.kt15.finomen.compiler.parser.expressions.ExpressionsParser {

}

class ExpressionsParserSpec  extends FlatSpec with Matchers {
  def context = new Context

  "ExpressionsParser" should "parse 1" in {
    val res = ExpressionsParser.parseAll(ExpressionsParser.expressionsParser(context), "1").get
    res shouldBe a [ScalarExpression]
    res.asInstanceOf[ScalarExpression].value shouldEqual (1)
  }

  "ExpressionsParser" should "parse variableName" in {
    val c = context
    c += new Variable("variableName", new Char(SignModifier.None))
    c.variables.contains("variableName") shouldEqual(true)
    val res = ExpressionsParser.parseAll(ExpressionsParser.expressionsParser(c), "variableName").get
    res shouldBe a [VariableAccessExpression]
    res.asInstanceOf[VariableAccessExpression].variable.name shouldEqual ("variableName")
  }

  "ExpressionsParser" should "fail undefinedVariableName" in {
    val c = context
    c += new Variable("variableName", new Char(SignModifier.None))
    val res = ExpressionsParser.parseAll(ExpressionsParser.expressionsParser(c), "undefinedVariableName")
    res shouldBe a [ExpressionsParser.NoSuccess]
  }

  "ExpressionsParser" should "parse -1" in {
    val res = ExpressionsParser.parseAll(ExpressionsParser.expressionsParser(context), "-1").get
    res shouldBe a [Operator]
    res.asInstanceOf[Operator].operator shouldEqual(OperatorType.UnaryMinus)
    res.asInstanceOf[Operator].arguments.size shouldEqual (1)
    res.asInstanceOf[Operator].arguments(0).left.get shouldBe a [ScalarExpression]
  }

  "ExpressionsParser" should "parse +1" in {
    val res = ExpressionsParser.parseAll(ExpressionsParser.expressionsParser(context), "+1").get
    res shouldBe a [Operator]
    res.asInstanceOf[Operator].operator shouldEqual(OperatorType.UnaryPlus)
    res.asInstanceOf[Operator].arguments.size shouldEqual (1)
    res.asInstanceOf[Operator].arguments(0).left.get shouldBe a [ScalarExpression]
  }

  "ExpressionsParser" should "parse --1" in {
    val res = ExpressionsParser.parseAll(ExpressionsParser.expressionsParser(context), "--1").get
    res shouldBe a [Operator]
    res.asInstanceOf[Operator].operator shouldEqual(OperatorType.PrefixDecrement)
    res.asInstanceOf[Operator].arguments.size shouldEqual (1)
    res.asInstanceOf[Operator].arguments(0).left.get shouldBe a [ScalarExpression]
  }

  "ExpressionsParser" should "parse ++1" in {
    val res = ExpressionsParser.parseAll(ExpressionsParser.expressionsParser(context), "++1").get
    res shouldBe a [Operator]
    res.asInstanceOf[Operator].operator shouldEqual(OperatorType.PrefixIncrement)
    res.asInstanceOf[Operator].arguments.size shouldEqual (1)
    res.asInstanceOf[Operator].arguments(0).left.get shouldBe a [ScalarExpression]
  }

  "ExpressionsParser" should "parse *1" in {
    val res = ExpressionsParser.parseAll(ExpressionsParser.expressionsParser(context), "*1").get
    res shouldBe a [Operator]
    res.asInstanceOf[Operator].operator shouldEqual(OperatorType.Dereference)
    res.asInstanceOf[Operator].arguments.size shouldEqual (1)
    res.asInstanceOf[Operator].arguments(0).left.get shouldBe a [ScalarExpression]
  }

  "ExpressionsParser" should "parse &1" in {
    val res = ExpressionsParser.parseAll(ExpressionsParser.expressionsParser(context), "&1").get
    res shouldBe a [Operator]
    res.asInstanceOf[Operator].operator shouldEqual(OperatorType.AddressOf)
    res.asInstanceOf[Operator].arguments.size shouldEqual (1)
    res.asInstanceOf[Operator].arguments(0).left.get shouldBe a [ScalarExpression]
  }

  "ExpressionsParser" should "parse !1" in {
    val res = ExpressionsParser.parseAll(ExpressionsParser.expressionsParser(context), "!1").get
    res shouldBe a [Operator]
    res.asInstanceOf[Operator].operator shouldEqual(OperatorType.LogicalNot)
    res.asInstanceOf[Operator].arguments.size shouldEqual (1)
    res.asInstanceOf[Operator].arguments(0).left.get shouldBe a [ScalarExpression]
  }

  "ExpressionsParser" should "parse ~1" in {
    val res = ExpressionsParser.parseAll(ExpressionsParser.expressionsParser(context), "~1").get
    res shouldBe a [Operator]
    res.asInstanceOf[Operator].operator shouldEqual(OperatorType.BitwiseNot)
    res.asInstanceOf[Operator].arguments.size shouldEqual (1)
    res.asInstanceOf[Operator].arguments(0).left.get shouldBe a [ScalarExpression]
  }

  "ExpressionsParser" should "parse !~1" in {
    val res = ExpressionsParser.parseAll(ExpressionsParser.expressionsParser(context), "!~1").get
    res shouldBe a [Operator]
    res.asInstanceOf[Operator].operator shouldEqual(OperatorType.LogicalNot)
    res.asInstanceOf[Operator].arguments.size shouldEqual (1)
    res.asInstanceOf[Operator].arguments(0).left.get shouldBe a [Operator]
  }

  "ExpressionsParser" should "parse 1++" in {
    val res = ExpressionsParser.parseAll(ExpressionsParser.expressionsParser(context), "1++").get
    res shouldBe a [Operator]
    res.asInstanceOf[Operator].operator shouldEqual(OperatorType.SuffixIncrement)
    res.asInstanceOf[Operator].arguments.size shouldEqual (1)
    res.asInstanceOf[Operator].arguments(0).left.get shouldBe a [ScalarExpression]
  }

  "ExpressionsParser" should "parse 1--" in {
    val res = ExpressionsParser.parseAll(ExpressionsParser.expressionsParser(context), "1--").get
    res shouldBe a [Operator]
    res.asInstanceOf[Operator].operator shouldEqual(OperatorType.SuffixDecrement)
    res.asInstanceOf[Operator].arguments.size shouldEqual (1)
    res.asInstanceOf[Operator].arguments(0).left.get shouldBe a [ScalarExpression]
  }

  "ExpressionsParser" should "parse 1[-2]" in {
    val res = ExpressionsParser.parseAll(ExpressionsParser.expressionsParser(context), "1[-2]").get
    res shouldBe a [Operator]
    res.asInstanceOf[Operator].operator shouldEqual(OperatorType.ArraySubscript)
    res.asInstanceOf[Operator].arguments.size shouldEqual (2)
    res.asInstanceOf[Operator].arguments(0).left.get shouldBe a [ScalarExpression]
    res.asInstanceOf[Operator].arguments(1).left.get shouldBe a [Operator]
  }

  "ExpressionsParser" should "parse 1(-2, 3)" in {
    val res = ExpressionsParser.parseAll(ExpressionsParser.expressionsParser(context), "1(-2, 3)").get
    res shouldBe a [Operator]
    res.asInstanceOf[Operator].operator shouldEqual(OperatorType.FunctionCall)
    res.asInstanceOf[Operator].arguments.size shouldEqual (3)
    res.asInstanceOf[Operator].arguments(0).left.get shouldBe a [ScalarExpression]
    res.asInstanceOf[Operator].arguments(1).left.get shouldBe a [Operator]
    res.asInstanceOf[Operator].arguments(2).left.get shouldBe a [ScalarExpression]
  }

  "ExpressionsParser" should "parse 1()" in {
    val res = ExpressionsParser.parseAll(ExpressionsParser.expressionsParser(context), "1()").get
    res shouldBe a [Operator]
    res.asInstanceOf[Operator].operator shouldEqual(OperatorType.FunctionCall)
    res.asInstanceOf[Operator].arguments.size shouldEqual (1)
    res.asInstanceOf[Operator].arguments(0).left.get shouldBe a [ScalarExpression]
  }

  "ExpressionsParser" should "parse 1*1" in {
    val res = ExpressionsParser.parseAll(ExpressionsParser.expressionsParser(context), "1*1").get
    res shouldBe a [Operator]
    res.asInstanceOf[Operator].operator shouldEqual(OperatorType.Multiplication)
  }

  "ExpressionsParser" should "parse 1*1/2" in {
    val res = ExpressionsParser.parseAll(ExpressionsParser.expressionsParser(context), "1*1/1").get
    res shouldBe a [Operator]
    res.asInstanceOf[Operator].operator shouldEqual(OperatorType.Division)
  }

  "ExpressionsParser" should "parse 1+1" in {
    val res = ExpressionsParser.parseAll(ExpressionsParser.expressionsParser(context), "1+1").get
    res shouldBe a [Operator]
    res.asInstanceOf[Operator].operator shouldEqual(OperatorType.Addition)
  }

  "ExpressionsParser" should "parse 1+ -1" in {
    val res = ExpressionsParser.parseAll(ExpressionsParser.expressionsParser(context), "1+1").get
    res shouldBe a [Operator]
    res.asInstanceOf[Operator].operator shouldEqual(OperatorType.Addition)
  }

  "ExpressionsParser" should "parse 1*1+1*1" in {
    val res = ExpressionsParser.parseAll(ExpressionsParser.expressionsParser(context), "1*1+1*1").get
    res shouldBe a [Operator]
    res.asInstanceOf[Operator].operator shouldEqual(OperatorType.Addition)
  }

  "ExpressionsParser" should "parse sizeof(1)" in {
    val res = ExpressionsParser.parseAll(ExpressionsParser.expressionsParser(context), "sizeof(1)").get
    res shouldBe a [Operator]
    res.asInstanceOf[Operator].operator shouldEqual(OperatorType.SizeOf)
    res.asInstanceOf[Operator].arguments.size shouldEqual (1)
    res.asInstanceOf[Operator].arguments(0).left.get shouldBe a [ScalarExpression]
  }

  "ExpressionsParser" should "parse sizeof(int)" in {
    val res = ExpressionsParser.parseAll(ExpressionsParser.expressionsParser(context), "sizeof(int)").get
    res shouldBe a [Operator]
    res.asInstanceOf[Operator].operator shouldEqual(OperatorType.SizeOf)
    res.asInstanceOf[Operator].arguments.size shouldEqual (1)
    res.asInstanceOf[Operator].arguments(0).right.get shouldBe a [Int]
  }

  "ExpressionsParser" should "parse 1 >>= 2 + 2" in {
    val res = ExpressionsParser.parseAll(ExpressionsParser.expressionsParser(context), "1 >>= 2 + 2").get
    res shouldBe a [Operator]
    res.asInstanceOf[Operator].operator shouldEqual(OperatorType.AssignmentRightShift)
  }

  "ExpressionsParser" should "parse 3 = 1 >>= 2 + 2" in {
    val res = ExpressionsParser.parseAll(ExpressionsParser.expressionsParser(context), "3 = 1 >>= 2 + 2").get
    res shouldBe a [Operator]
    res.asInstanceOf[Operator].operator shouldEqual(OperatorType.Assignment)
  }

  "ExpressionsParser" should "parse 1 ? 2 : 3" in {
    val res = ExpressionsParser.parseAll(ExpressionsParser.expressionsParser(context), "1 ? 2 : 3").get
    res shouldBe a [Operator]
    res.asInstanceOf[Operator].operator shouldEqual(OperatorType.Ternary)
  }

  "ExpressionsParser" should "parse -1 + -(2+2)" in {
    val res = ExpressionsParser.parseAll(ExpressionsParser.expressionsParser(context), "-1 + -(2+2)").get
    res shouldBe a [Operator]
  }
}