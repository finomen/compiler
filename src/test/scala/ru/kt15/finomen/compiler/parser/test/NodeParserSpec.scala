package ru.kt15.finomen.compiler.parser.test

import ru.kt15.finomen.compiler.ast.types.builtin.{SizeModifier, SignModifier, Int}
import org.scalatest.{Matchers, FlatSpec}
import ru.kt15.finomen.compiler.ast.nodes._
import ru.kt15.finomen.compiler.ast.nodes.TypedefNode
import ru.kt15.finomen.compiler.ast.nodes.ExpressionNode
import ru.kt15.finomen.compiler.ast.nodes.BlockNode
import ru.kt15.finomen.compiler.ast.nodes.PseudoBlockNode
import ru.kt15.finomen.compiler.ast.types.builtin.Int
import ru.kt15.finomen.compiler.env.Context

/**
 * Created by finomen on 07.04.14.
 */

object NodesParser extends AnyRef with ru.kt15.finomen.compiler.parser.nodes.NodesParser {

}

class NodeParserSpec  extends FlatSpec with Matchers {
  def context = new Context
  "TypedefNodeParser" should "parse typedef int newType;" in {
    val c = context
    val res = NodesParser.parseAll(NodesParser.nodesParser(c), "typedef int newType;").get
    res shouldBe a [TypedefNode]
    res.asInstanceOf[TypedefNode].innerType shouldBe a [Int]
    res.asInstanceOf[TypedefNode].name shouldEqual("newType")
    res.asInstanceOf[TypedefNode].innerType shouldEqual(c.typedefs("newType"))
  }

  "TypedefNodeParser" should "parse 2+2;" in {
    val res = NodesParser.parseAll(NodesParser.nodesParser(context), "2+2;").get
    res shouldBe a [ExpressionNode]
  }

  "BlockNodeParser" should "parse {2+2;}" in {
    val res = NodesParser.parseAll(NodesParser.nodesParser(context), "{2+2;}").get
    res shouldBe a [BlockNode]
  }

  "VariableDeclarationNodeParser" should "parse int a, *b, ***c;" in {
    val res = NodesParser.parseAll(NodesParser.nodesParser(context), "int a, *b, ***c;").get
    res shouldBe a [PseudoBlockNode]
  }

  "VariableDeclarationNodeParser" should "parse int b = 2 + 2 * 2, b2 = (2 + 2) * 2;" in {
    val res = NodesParser.parseAll(NodesParser.nodesParser(context), "int b = 2 + 2 * 2, b2 = (2 + 2) * 2;").get
    res shouldBe a [PseudoBlockNode]
  }

  "VariableDeclarationNodeParser" should "parse int a = 0, *b, ***c;" in {
    val res = NodesParser.parseAll(NodesParser.nodesParser(context), "int a = 0, *b, ***c;").get
    res shouldBe a [PseudoBlockNode]
  }

  "IfNodeParser" should "parse if (1) 2;" in {
    val res = NodesParser.parseAll(NodesParser.nodesParser(context), "if (1) 2;").get
    res shouldBe a [IfNode]
  }

  "IfNodeParser" should "parse if (1) 2; else {}" in {
    val res = NodesParser.parseAll(NodesParser.nodesParser(context), "if (1) 2; else {}").get
    res shouldBe a [IfNode]
  }

  "ForNodeParser" should "parse for(;;);" in {
    val res = NodesParser.parseAll(NodesParser.nodesParser(context), "for(;;);").get
    res shouldBe a [ForNode]
  }

  "ForNodeParser" should "parse for(0;0;0);" in {
    val res = NodesParser.parseAll(NodesParser.nodesParser(context), "for(0;0;0);").get
    res shouldBe a [ForNode]
  }

  "ForNodeParser" should "parse for(int a = 0;0;0);" in {
    val c = context
    val res = NodesParser.parseAll(NodesParser.nodesParser(c), "for(int a = 0;0;0);").get
    c.variables.contains("a") shouldEqual (false)
    res shouldBe a [ForNode]
  }

  "ForNodeParser" should "parse for(int a = 0;a > 0; a++);" in {
    val c = context
    val res = NodesParser.parseAll(NodesParser.nodesParser(c), "for(int a = 0; a > 0; a++);").get
    res shouldBe a [ForNode]
    c.variables.contains("a") shouldEqual (false)
  }

  "ForNodeParser" should "parse for(int a = 0, int b = 10;a > b; a++, b--);" in {
    val c = context
    val res = NodesParser.parseAll(NodesParser.nodesParser(c), "for(int a = 0, int b = 10;a > b; a++, b--);").get
    res shouldBe a [ForNode]
    c.variables.contains("a") shouldEqual (false)
    c.variables.contains("b") shouldEqual (false)
  }

  "ForNodeParser" should "not parse for(int a = 0;a > 0; b++);" in {
    val res = NodesParser.parseAll(NodesParser.nodesParser(context), "for(int a = 0; a > 0; b++);")
    res shouldBe a [NodesParser.NoSuccess]
  }

  "WhileNodeParser" should "parse while(1);" in {
    val res = NodesParser.parseAll(NodesParser.nodesParser(context), "while(1);").get
    res shouldBe a [WhileNode]
  }

  "WhileNodeParser" should "parse while(1) 0+2;" in {
    val res = NodesParser.parseAll(NodesParser.nodesParser(context), "while(1);").get
    res shouldBe a [WhileNode]
  }

  "DoWhileNodeParser" should "parse do 0+2; while (1);" in {
    val res = NodesParser.parseAll(NodesParser.nodesParser(context), "do 0+2; while (1);").get
    res shouldBe a [DoWhileNode]
  }

  "FunctionNodeParser" should "parse void foo();" in {
    val res = NodesParser.parseAll(NodesParser.rootNodesParser(context), "void foo();").get
    res shouldBe a [FunctionNode]
  }

  "FunctionNodeParser" should "parse void foo(int a);" in {
    val res = NodesParser.parseAll(NodesParser.rootNodesParser(context), "void foo(int a);").get
    res shouldBe a [FunctionNode]
  }

  "FunctionNodeParser" should "parse void foo(int a, int b);" in {
    val res = NodesParser.parseAll(NodesParser.rootNodesParser(context), "void foo(int a);").get
    res shouldBe a [FunctionNode]
  }

  "FunctionNodeParser" should "parse void foo(int a = 0);" in {
    val res = NodesParser.parseAll(NodesParser.rootNodesParser(context), "void foo(int a = 0);").get
    res shouldBe a [FunctionNode]
  }

  "FunctionNodeParser" should "parse void foo(int a[]);" in {
    val res = NodesParser.parseAll(NodesParser.rootNodesParser(context), "void foo(int a[]);").get
    res shouldBe a [FunctionNode]
  }

  "ReturnNodeParser" should "parse return 2;" in {
    val res = NodesParser.parseAll(NodesParser.nodesParser(context), "return 2;").get
    res shouldBe a [ReturnNode]
  }

  "ReturnNodeParser" should "parse return;" in {
    val res = NodesParser.parseAll(NodesParser.nodesParser(context), "return;").get
    res shouldBe a [ReturnNode]
  }

  "FunctionNodeParser" should "parse void foo(int a = 0) {a++;}" in {
    val c = (context)
    val res = NodesParser.parseAll(NodesParser.rootNodesParser(c), "void foo(int a = 0) {a++;}").get
    res shouldBe a [FunctionNode]
    c.functions.contains("foo") shouldEqual (true)
    c.variables.contains("a") shouldEqual (false)
  }

  "FunctionNodeParser" should "parse void foo(int a) {a++;}" in {
    val c = (context)
    val res = NodesParser.parseAll(NodesParser.rootNodesParser(c), "void foo(int a) {a++;}").get
    res shouldBe a [FunctionNode]
    c.functions.contains("foo") shouldEqual (true)
    c.variables.contains("a") shouldEqual (false)
  }

  "FunctionNodeParser" should "parse void foo(int a = 0) {}" in {
    val c = (context)
    val res = NodesParser.parseAll(NodesParser.rootNodesParser(c), "void foo(int a = 0) {}").get
    res shouldBe a [FunctionNode]
    c.functions.contains("foo") shouldEqual (true)
    c.variables.contains("a") shouldEqual (false)
  }
}
