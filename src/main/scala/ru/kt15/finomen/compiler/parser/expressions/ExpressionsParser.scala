package ru.kt15.finomen.compiler.parser.expressions

import scala.util.parsing.combinator.{PackratParsers, JavaTokenParsers}
import ru.kt15.finomen.compiler.ast.expressions._
import ru.kt15.finomen.compiler.ast.expressions.operators.{OperatorType, Operator}
import ru.kt15.finomen.compiler.ast.expressions.operators.OperatorType.OperatorType
import ru.kt15.finomen.compiler.ast.types.{AliasType, CType}
import ru.kt15.finomen.compiler.parser.types.CTypeParser
import ru.kt15.finomen.compiler.parser.{ContextParser, IdentifierParser}
import ru.kt15.finomen.compiler.ast.expressions.InParens
import ru.kt15.finomen.compiler.ast.expressions.operators.Operator
import ru.kt15.finomen.compiler.ast.expressions.VariableAccessExpression
import ru.kt15.finomen.compiler.ast.expressions.ScalarExpression
import ru.kt15.finomen.compiler.env.Context

/**
 * Created by finomen on 07.04.14.
 */
trait ExpressionsParser extends AnyRef with JavaTokenParsers with PackratParsers with CTypeParser with ContextParser {
  lazy val binaryOperatorsByPriorities : List[List[(OperatorType, String)]] = List[List[(OperatorType, String)]](
    List[(OperatorType, String)](
      (OperatorType.Multiplication, "*"),
      (OperatorType.Division, "/"),
      (OperatorType.Modulo, "%")
    ),
    List[(OperatorType, String)](
      (OperatorType.Addition, "+"),
      (OperatorType.Subtraction, "-")
    ),
    List[(OperatorType, String)](
      (OperatorType.BitwiseLeftShift, "<<"),
      (OperatorType.BitwiseRightShift, ">>")
    ),
    List[(OperatorType, String)](
      (OperatorType.Less, "<"),
      (OperatorType.LessOrEqual, "<="),
      (OperatorType.Greater, ">"),
      (OperatorType.GreaterOrEqual, ">=")
    ),
    List[(OperatorType, String)](
      (OperatorType.Equal, "=="),
      (OperatorType.NotEqual, "!=")
    ),
    List[(OperatorType, String)](
      (OperatorType.BitwiseAnd, "&")
    ),
    List[(OperatorType, String)](
      (OperatorType.BitwiseXor, "^")
    ),
    List[(OperatorType, String)](
      (OperatorType.BitwiseOr, "|")
    ),
    List[(OperatorType, String)](
      (OperatorType.LogicalAnd, "&&")
    ),
    List[(OperatorType, String)](
      (OperatorType.LogicalOr, "||")
    )
  )

  lazy val intScalar : PackratParser[Expression] = "[0-9]+".r ^^ {x => new ScalarExpression(x.toInt)}
  lazy val longScalar : PackratParser[Expression] = "[0-9]+l".r ^^ {x => new ScalarExpression(x.toLong)}

  lazy val stringScalar : PackratParser[Expression] = "\"" ~> "[^\"]*".r <~ "\"" ^^ {x => new ScalarExpression(x)}
  lazy val charScalar : PackratParser[Expression] = "\'" ~> "[^\"]".r <~ "\'" ^^ {x => new ScalarExpression(x.head)}

  lazy val doubleScalar : PackratParser[Expression] = ("[0-9]+\\.[0-9]*".r | "\\.[0-9]+".r) ^^ {x => new ScalarExpression(x.toDouble)}

  lazy val floatScalar : PackratParser[Expression] = ("[0-9]+\\.[0-9]*f".r | "\\.?[0-9]+f".r) ^^ {x => new ScalarExpression(x.toFloat)}



  lazy val scalar : PackratParser[Expression] = /*floatScalar | doubleScalar |*/ longScalar | intScalar | stringScalar | charScalar

  lazy val parens : PackratParser[Expression] = "(" ~ expressionsParser ~ ")" ^^ {x => new InParens(x._1._2)}

  lazy val variableAccess : PackratParser[Expression] = identifierParser ^?
    {
      case x if (context.variables.contains(x)) => new VariableAccessExpression(context.variables.get(x).get)
    }

  lazy val functionAccess : PackratParser[Expression] = identifierParser ^?
    {
      case x if (context.functions.contains(x)) => new FunctionAccessExpression(x)
    }

  lazy val valueAccess : PackratParser[Expression] = variableAccess | scalar

  lazy val suffixOperatorImpl : PackratParser[Expression] = suffixOperator ~ ("++" | "--") ^^
    {x => new Operator(
      x._2 match {
        case "++" => OperatorType.SuffixIncrement
        case "--" => OperatorType.SuffixDecrement
      }
      , List[Either[Expression, CType]](Left(x._1)))}

  lazy val arrayAccess : PackratParser[Expression] = suffixOperator ~ "[" ~ expressionsParser ~ "]" ^^
    {x => new Operator(OperatorType.ArraySubscript, List[Either[Expression, CType]](Left(x._1._1._1), Left(x._1._2)))}

  lazy val call : PackratParser[Expression] = (suffixOperator | functionAccess) ~ "(" ~ rep1sep(noCommaExpression, ",").? ~ ")" ^^
    {x => new Operator(OperatorType.FunctionCall, List[Either[Expression, CType]](Left(x._1._1._1))
    ++
      x._1._2.getOrElse(List[Expression]()).map(e => Left(e))
    )}

  lazy val suffixOperator : PackratParser[Expression] = suffixOperatorImpl | arrayAccess | call | valueAccess | parens

  lazy val cast : PackratParser[Expression] = "(" ~ cTypeParser(context) ~ ")" ~ prefixOperator ^^
    {x => new Operator(OperatorType.Cast, List[Either[Expression, CType]](Right(x._1._1._2), Left(x._2)))}

  lazy val sizeof : PackratParser[Expression] = "sizeof" ~ "(" ~
    ((cTypeParser(context) ^^ {x => Right(x)}) | (expressionsParser ^^ {x => Left(x)})) ~ ")" ^^
    {x => new Operator(OperatorType.SizeOf, List[Either[Expression, CType]](x._1._2))}

  lazy val prefixOperatorImpl : PackratParser[Expression] = ("++" | "--" | "-" | "+" | "~" | "!" | "*" | " &") ~ (prefixOperator) ^^
    {x => new Operator(
      x._1 match {
        case "++" => OperatorType.PrefixIncrement
        case "--" => OperatorType.PrefixDecrement
        case "+" => OperatorType.UnaryPlus
        case "-" => OperatorType.UnaryMinus
        case "~" => OperatorType.BitwiseNot
        case "!" => OperatorType.LogicalNot
        case "*" => OperatorType.Dereference
        case " &" => OperatorType.AddressOf
      }
    , List[Either[Expression, CType]](Left(x._2)))}

  lazy val prefixOperator : PackratParser[Expression] = cast | prefixOperatorImpl | sizeof | suffixOperator

  lazy val binaryOperator : PackratParser[Expression] = {
    val groupedOps = binaryOperatorsByPriorities.map{x =>
      val ops = x.map(y => y._2 ^^ {z => y._1})
      ops.tail.foldLeft(ops.head){(l ,r) => l | r}
    }

    groupedOps.foldLeft(prefixOperator)((l, r) => l ~ (rep(r ~ l)) ^^ {x =>
      x._2.foldLeft(x._1){(l, r) => new Operator(r._1, List[Either[Expression, CType]](Left(l), Left(r._2)))}
    })
  }


  lazy val assignmentExpression: PackratParser[Expression] = (binaryOperator ~ ("[-+*/%&^|]=".r | ">>=" | "<<=" | "=") ~ assignmentExpression ^^
    {x => new Operator(
      x._1._2 match {
        case "=" => OperatorType.Assignment
        case "+=" => OperatorType.AssignmentSum
        case "-=" => OperatorType.AssignmentDifference
        case "*=" => OperatorType.AssignmentProduct
        case "/=" => OperatorType.AssignmentQuotient
        case "%=" => OperatorType.AssignmentRemainder
        case "<<=" => OperatorType.AssignmentLeftShift
        case ">>=" => OperatorType.AssignmentRightShift
        case "&=" => OperatorType.AssignmentBitwiseAnd
        case "^=" => OperatorType.AssignmentBitwiseXor
        case "|=" => OperatorType.AssignmentBitwiseOr
      }, List[Either[Expression, CType]](Left(x._1._1), Left(x._2)))}) | binaryOperator

  lazy val ternaryExpression : PackratParser[Operator] = (binaryOperator ~ "?" ~ binaryOperator ~ ":" ~ binaryOperator) ^^
    {x => new Operator(OperatorType.Ternary, List[Either[Expression, CType]](Left(x._1._1._1._1), Left(x._1._1._2), Left(x._2)))}

  lazy val commaExpression : PackratParser[Operator] = (noCommaExpression ~ "," ~ expressionsParser) ^^
    {x => new Operator(OperatorType.Comma, List[Either[Expression, CType]](Left(x._1._1), Left(x._2)))}

  lazy val noCommaExpression : PackratParser[Expression] = ternaryExpression | assignmentExpression

  lazy val expressionsParser : PackratParser[Expression] = commaExpression | noCommaExpression

  def noCommaExpression(cContext : Context) : PackratParser[Expression] = setContext(cContext) ~> noCommaExpression
  def expressionsParser(cContext : Context) : PackratParser[Expression] = setContext(cContext) ~> expressionsParser
}
