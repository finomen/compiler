package ru.kt15.finomen.compiler.parser.types

import scala.util.parsing.combinator.{PackratParsers, JavaTokenParsers}
import ru.kt15.finomen.compiler.ast.types.{CType, ArrayType}

/**
 * Created by finomen on 02.04.14.
 */
trait ArrayTypeParser extends AnyRef with JavaTokenParsers with PackratParsers  {
  def cTypeParser : PackratParser[CType]
  lazy val arrayTypeParser : PackratParser[ArrayType] = (cTypeParser ~ "[" ~ "]") ^^ (x => new ArrayType(x._1._1))
}
