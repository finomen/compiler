package ru.kt15.finomen.compiler.parser.nodes

import scala.util.parsing.combinator.{PackratParsers, JavaTokenParsers}
import ru.kt15.finomen.compiler.parser.types.CTypeParser
import ru.kt15.finomen.compiler.ast.nodes.{Node, PseudoBlockNode, VariableDeclarationNode, TypedefNode}
import ru.kt15.finomen.compiler.parser.IdentifierParser
import ru.kt15.finomen.compiler.ast.types.CType
import ru.kt15.finomen.compiler.parser.expressions.ExpressionsParser
import ru.kt15.finomen.compiler.ast.types.pointers.PointerType
import ru.kt15.finomen.compiler.env.{Variable, Context}

/**
 * Created by finomen on 07.04.14.
 */
trait VariableDeclarationNodeParser extends AnyRef with JavaTokenParsers with PackratParsers with CTypeParser with IdentifierParser with ExpressionsParser {
  def variableDeclarationNodeParserImpl(context : Context, baseType : CType) : Parser[VariableDeclarationNode] =
    (identifierParser ~ ("=" ~> noCommaExpression(context)).?) ^?
      {case x if !context.variables.contains(x._1) => {
        val v = new VariableDeclarationNode(new Variable(x._1, baseType), x._2)
        context += v.variable
        v
      }
      }

  def variableDeclarationNodeParser(context : Context) : Parser[Node] = nonPointerTypeParser(context) >> {x => rep1sep(rep("*") >> {
    y => variableDeclarationNodeParserImpl(context, y.foldLeft(x) {(a, b) => new PointerType(a)})
  }, ",")} <~ ";" ^^ { x => new PseudoBlockNode(x)}

  def singleVariableDeclarationNodeParser(context : Context) : Parser[VariableDeclarationNode] = nonPointerTypeParser(context) >> {x => rep("*") >> {
    y => variableDeclarationNodeParserImpl(context, y.foldLeft(x) {(a, b) => new PointerType(a)})
  }}

}
