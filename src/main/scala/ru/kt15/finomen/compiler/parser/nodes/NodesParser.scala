package ru.kt15.finomen.compiler.parser.nodes

import scala.util.parsing.combinator.{PackratParsers, JavaTokenParsers}
import ru.kt15.finomen.compiler.ast.nodes.{PseudoBlockNode, Node}
import ru.kt15.finomen.compiler.parser.ContextParser
import ru.kt15.finomen.compiler.env.Context

/**
 * Created by finomen on 07.04.14.
 */

trait NodesParser extends AnyRef with JavaTokenParsers with PackratParsers with TypedefNodeParser with VariableDeclarationNodeParser  with ExpressionNodeParser with IfNodeParser with BlockNodeParser with ForNodeParser with WhileNodeParser with DoWhileNodeParser with FunctionNodeParser with ReturnNodeParser with ContextParser{
  protected override val whiteSpace = """(\s|//.*|(?m)/\*(\*(?!/)|[^*])*\*/)+""".r
  def nodesParser(context : Context) : Parser[Node] = typedefNodeParser(context) |
    variableDeclarationNodeParser(context) |
    expressionNodeParser(context) |
    ifNodeParser(context) |
    blockNodeParser(context) |
    forNodeParser(context) |
    whileNodeParser(context) |
    doWhileNodeParser(context) |
    returnNodeParser(context)


  def rootNodesParser(context : Context) : PackratParser[Node] = typedefNodeParser(context) | functionNodeParser(context) | variableDeclarationNodeParser(context)

  def  sourceParser : PackratParser[PseudoBlockNode] = newContext >> {context => rep(rootNodesParser(context)) ^^ {x => new PseudoBlockNode(x)}}
}
