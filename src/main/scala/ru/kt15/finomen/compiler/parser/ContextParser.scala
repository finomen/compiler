package ru.kt15.finomen.compiler.parser

import scala.util.parsing.combinator.{PackratParsers, JavaTokenParsers}
import ru.kt15.finomen.compiler.env.{Variable, Context}
import ru.kt15.finomen.compiler.ast.types.CType
import ru.kt15.finomen.compiler.env.Function

/**
 * Created by finom_000 on 19.04.2014.
 */
trait ContextParser  extends AnyRef with JavaTokenParsers with PackratParsers {
  def newContext : Parser[Context] = "" ^^ {x => new Context()}
  def pushContext(context : Context) : Parser[Context] = "" ^^ {x => context.push}
  def pushFunction(context : Context, name : String, returnType : Option[CType], args : List[Variable]) : Parser[Function] = "" ^^ {x => context.pushFunction(name, returnType, args)}
  def setContext(cContext : Context) : Parser[Unit] = "" ^^ {x => ContextParser.context = cContext}

  def context = ContextParser.context
}

object ContextParser {
  var context : Context = new Context
}