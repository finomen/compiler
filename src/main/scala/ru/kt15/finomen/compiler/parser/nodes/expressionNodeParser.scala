package ru.kt15.finomen.compiler.parser.nodes

import scala.util.parsing.combinator.{PackratParsers, JavaTokenParsers}
import ru.kt15.finomen.compiler.parser.types.CTypeParser
import ru.kt15.finomen.compiler.parser.IdentifierParser
import ru.kt15.finomen.compiler.ast.nodes.ExpressionNode
import ru.kt15.finomen.compiler.parser.expressions.ExpressionsParser
import ru.kt15.finomen.compiler.env.Context

/**
 * Created by finom_000 on 11.04.2014.
 */
trait ExpressionNodeParser  extends AnyRef with JavaTokenParsers with PackratParsers with CTypeParser with IdentifierParser with ExpressionsParser{

  def expressionNodeParser(context : Context) : PackratParser[ExpressionNode] = (expressionsParser(context) ~ ";") ^^ {x =>
    new ExpressionNode(x._1)
  }
}
