package ru.kt15.finomen.compiler.parser

import scala.util.parsing.combinator.{PackratParsers, JavaTokenParsers}

/**
 * Created by finomen on 07.04.14.
 */
trait IdentifierParser extends AnyRef with JavaTokenParsers with PackratParsers {
  def keywords : List[String] = List[String](
    "auto", "_Bool", "break", "case", "char", "_Complex", "const", "continue", "default", "do",
    "double", "else", "enum", "extern", "float", "for", "goto", "if", "_Imaginary", "inline",
    "int", "long", "register", "restrict", "return", "short", "signed", "sizeof", "static",
    "struct", "switch", "typedef", "union", "unsigned", "void", "volatile", "while"
    )

  def reservedParser : Parser[String] = keywords.tail.foldLeft(Parser[String](keywords.head)){(x, y) => x | y}
  def identifierParser : Parser[String] = not(reservedParser) ~> "[a-zA-Z_][a-zA-Z0-9_]*".r

}
