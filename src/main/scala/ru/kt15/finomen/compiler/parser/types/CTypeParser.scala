
package ru.kt15.finomen.compiler.parser.types
import scala.util.parsing.combinator.{PackratParsers, JavaTokenParsers}
import ru.kt15.finomen.compiler.ast.types.CType
import ru.kt15.finomen.compiler.parser.ContextParser
import ru.kt15.finomen.compiler.env.Context

/**
 * Created by finomen on 02.04.14.
 */
trait CTypeParser extends AnyRef with JavaTokenParsers with PackratParsers with ContextParser
    with ArrayTypeParser
    with BuiltInTypeParser
    with AliasTypeParser
    with PointerTypeParser
    with ConstTypeParser {


  lazy val cTypeParser : PackratParser[CType] = pointerTypeParser | nonPointerTypeParser

  lazy val nonPointerTypeParser : PackratParser[CType] =  arrayTypeParser | constTypeParser | builtInTypeParser | aliasTypeParser

  def cTypeParser(context : Context) : PackratParser[CType] = setContext(context) ~> cTypeParser
  def nonPointerTypeParser(context : Context) : PackratParser[CType] = setContext(context) ~> nonPointerTypeParser
}
