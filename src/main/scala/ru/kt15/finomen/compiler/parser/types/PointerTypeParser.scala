package ru.kt15.finomen.compiler.parser.types

import scala.util.parsing.combinator.{PackratParsers, JavaTokenParsers}
import ru.kt15.finomen.compiler.ast.types.CType
import ru.kt15.finomen.compiler.ast.types.pointers._

/**
 * Created by finomen on 02.04.14.
 */
trait PointerTypeParser extends AnyRef with JavaTokenParsers with PackratParsers {
  def cTypeParser : PackratParser[CType]
  def nonPointerTypeParser : PackratParser[CType]

  lazy val pointerTypeParser : PackratParser[CType] = voidPointerParser | valuePointerParser
  lazy val voidPointerParser : Parser[CType] = ("void" ~ "*") ^^ (x => new VoidPointer())
  lazy val valuePointerParser : PackratParser[CType] = (cTypeParser ~ "*") ^^ (x => new PointerType(x._1))
}
