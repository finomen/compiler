package ru.kt15.finomen.compiler.parser.nodes

import scala.util.parsing.combinator.{PackratParsers, JavaTokenParsers}
import ru.kt15.finomen.compiler.parser.types.CTypeParser
import ru.kt15.finomen.compiler.parser.{ContextParser, IdentifierParser}
import ru.kt15.finomen.compiler.ast.nodes.{BlockNode, Node, ExpressionNode}
import ru.kt15.finomen.compiler.parser.expressions.ExpressionsParser
import ru.kt15.finomen.compiler.env.Context

/**
 * Created by finom_000 on 11.04.2014.
 */
trait BlockNodeParser  extends AnyRef with JavaTokenParsers with PackratParsers with CTypeParser with IdentifierParser with ExpressionsParser with ContextParser{
  def nodesParser(context : Context) : Parser[Node]

  def blockNodeParser(oldContext : Context) : PackratParser[BlockNode] = "{" ~> (pushContext(oldContext) >> {x => rep(nodesParser(x))}) <~ "}" ^^ {x => new BlockNode(x)}

}
