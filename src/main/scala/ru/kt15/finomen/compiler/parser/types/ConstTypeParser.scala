package ru.kt15.finomen.compiler.parser.types

import ru.kt15.finomen.compiler.ast.types.{ConstType, CType}
import scala.util.parsing.combinator.{PackratParsers, JavaTokenParsers}

/**
 * Created by finomen on 02.04.14.
 */
trait ConstTypeParser extends AnyRef with JavaTokenParsers with PackratParsers {
  def nonPointerTypeParser : PackratParser[CType]
  //FIXME: check double const
  lazy val constTypeParser : PackratParser[ConstType] = ("const" ~ not("const") ~ nonPointerTypeParser) ^^ (x => new ConstType(x._2))
}
