package ru.kt15.finomen.compiler.parser.types

import scala.util.parsing.combinator.{PackratParsers, JavaTokenParsers}
import ru.kt15.finomen.compiler.ast.types.builtin._
import ru.kt15.finomen.compiler.ast.types.builtin.SignModifier._
import ru.kt15.finomen.compiler.ast.types.builtin.SizeModifier._

/**
 * Created by finom_000 on 27.03.14.
 */
trait BuiltInTypeParser extends AnyRef with JavaTokenParsers with PackratParsers {
  lazy val builtInTypeParser : Parser[BuiltInType] = charParser /*| floatParser | doubleParser*/ | intParser | intShortFormParser

  lazy val doubleParser : Parser[BuiltInType] =
    ((longParser.? ^^ (x => x.getOrElse(SizeModifier.None))) ~ "double") ^^
      (x => new Double(x._1))

  lazy val floatParser : Parser[BuiltInType] =
    "float" ^^(x => new Float())

  lazy val charParser : Parser[BuiltInType] =
    (((signedParser | unsignedParser).? ^^ (x => x.getOrElse(SignModifier.Signed))) ~ "char") ^^
      (x => new Char(x._1))

  lazy val intParser : Parser[BuiltInType] =
    (((signedParser | unsignedParser).? ^^ (x => x.getOrElse(SignModifier.Signed))) ~
    ((longParser | shortParser).? ^^ (x => x.getOrElse(SizeModifier.None))) ~ "int") ^^
    (x => new Int(x._1._1, x._1._2))

  lazy val intShortFormParser : Parser[BuiltInType] =  (((signedParser | unsignedParser).? ^^ (x => x.getOrElse(SignModifier.Signed))) ~
    (longParser | shortParser)) ^^
    (x => new Int(x._1, x._2))

  lazy val longParser : Parser[SizeModifier] = "long" ^^ (x => SizeModifier.Long)
  lazy val shortParser : Parser[SizeModifier] = "short" ^^ (x => SizeModifier.Short)
  lazy val signedParser : Parser[SignModifier] = "signed" ^^ (x => SignModifier.Signed)
  lazy val unsignedParser : Parser[SignModifier] = "unsigned" ^^ (x => SignModifier.Unsigned)

}
