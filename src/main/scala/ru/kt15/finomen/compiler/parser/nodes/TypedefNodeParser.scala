package ru.kt15.finomen.compiler.parser.nodes

import scala.util.parsing.combinator.{PackratParsers, JavaTokenParsers}
import ru.kt15.finomen.compiler.parser.types.CTypeParser
import ru.kt15.finomen.compiler.ast.nodes.TypedefNode
import ru.kt15.finomen.compiler.parser.IdentifierParser
import ru.kt15.finomen.compiler.env.Context

/**
 * Created by finomen on 07.04.14.
 */
trait TypedefNodeParser extends AnyRef with JavaTokenParsers with PackratParsers with CTypeParser with IdentifierParser {
  def typedefNodeParserImpl(context : Context) : Parser[TypedefNode] = ("typedef" ~ cTypeParser(context) ~ identifierParser ~ ";") ^^
      {x => new TypedefNode(x._1._1._2, x._1._2)}

  def typedefNodeParser(context : Context) : Parser[TypedefNode] = typedefNodeParserImpl(context) ^^ {x =>
    context += (x.name -> x.innerType)
    x
  }
}
