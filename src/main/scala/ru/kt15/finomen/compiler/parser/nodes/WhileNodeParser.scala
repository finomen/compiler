package ru.kt15.finomen.compiler.parser.nodes

import scala.util.parsing.combinator.{PackratParsers, JavaTokenParsers}
import ru.kt15.finomen.compiler.parser.types.CTypeParser
import ru.kt15.finomen.compiler.parser.IdentifierParser
import ru.kt15.finomen.compiler.ast.nodes._
import ru.kt15.finomen.compiler.parser.expressions.ExpressionsParser
import ru.kt15.finomen.compiler.ast.nodes.ForNode
import ru.kt15.finomen.compiler.ast.nodes.IfNode
import ru.kt15.finomen.compiler.ast.expressions.Expression
import ru.kt15.finomen.compiler.env.Context

/**
 * Created by finom_000 on 11.04.2014.
 */
trait WhileNodeParser  extends AnyRef with JavaTokenParsers with PackratParsers with CTypeParser with IdentifierParser with ExpressionsParser with VariableDeclarationNodeParser{
  def nodesParser(context : Context) : Parser[Node]

  def whileNodeParser(context : Context) : PackratParser[WhileNode] = ("while" ~> ("(" ~> (expressionsParser(context) <~ ")")) ~ (nodesParser(context) ^^ {x => Some(x)} | (";" ^^ {x => None}))) ^^
    {
      x =>
        new WhileNode(x._1, x._2)
    }
  
}
