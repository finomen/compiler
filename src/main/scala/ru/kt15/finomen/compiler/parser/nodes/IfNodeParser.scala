package ru.kt15.finomen.compiler.parser.nodes

import scala.util.parsing.combinator.{PackratParsers, JavaTokenParsers}
import ru.kt15.finomen.compiler.parser.types.CTypeParser
import ru.kt15.finomen.compiler.parser.IdentifierParser
import ru.kt15.finomen.compiler.ast.nodes.{IfNode, Node, ExpressionNode}
import ru.kt15.finomen.compiler.parser.expressions.ExpressionsParser
import ru.kt15.finomen.compiler.env.Context

/**
 * Created by finom_000 on 11.04.2014.
 */
trait IfNodeParser  extends AnyRef with JavaTokenParsers with PackratParsers with CTypeParser with IdentifierParser with ExpressionsParser {
  def nodesParser(context : Context) : Parser[Node]

  def ifNodeParser(context : Context) : PackratParser[IfNode] = ("if" ~> ("(" ~> expressionsParser(context) <~ ")") ~ nodesParser(context) ~ ("else" ~> nodesParser(context)).?) ^^
    {
      x =>
        new IfNode(x._1._1, x._1._2, x._2)
    }
  
}
