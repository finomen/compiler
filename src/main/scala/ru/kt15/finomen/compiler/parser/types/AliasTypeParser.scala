package ru.kt15.finomen.compiler.parser.types

import scala.util.parsing.combinator.{PackratParsers, JavaTokenParsers}
import ru.kt15.finomen.compiler.ast.types.AliasType
import ru.kt15.finomen.compiler.parser.{ContextParser, IdentifierParser}

/**
 * Created by finomen on 02.04.14.
 */
trait AliasTypeParser extends AnyRef with JavaTokenParsers with PackratParsers with IdentifierParser with ContextParser {
  def aliasTypeParser : Parser[AliasType] = identifierParser ^? {
    case x if context.typedefs.contains(x) => new AliasType(context.typedefs(x))
  }
}
