package ru.kt15.finomen.compiler.parser.nodes

import scala.util.parsing.combinator.{PackratParsers, JavaTokenParsers}
import ru.kt15.finomen.compiler.parser.types.CTypeParser
import ru.kt15.finomen.compiler.parser.{ContextParser, IdentifierParser}
import ru.kt15.finomen.compiler.ast.nodes._
import ru.kt15.finomen.compiler.parser.expressions.ExpressionsParser
import ru.kt15.finomen.compiler.ast.nodes.ForNode
import ru.kt15.finomen.compiler.ast.nodes.IfNode
import ru.kt15.finomen.compiler.ast.expressions.Expression
import ru.kt15.finomen.compiler.env.Context

/**
 * Created by finom_000 on 11.04.2014.
 */
trait ForNodeParser  extends AnyRef with JavaTokenParsers with PackratParsers with CTypeParser with IdentifierParser with ExpressionsParser with VariableDeclarationNodeParser with ContextParser {
  def nodesParser(context : Context) : Parser[Node]

  def forExpr1(context : Context) : PackratParser[Either[Expression, List[VariableDeclarationNode]]] = rep1sep(singleVariableDeclarationNodeParser(context), ",") ^^ {x => Right(x)} | expressionsParser(context) ^^ {x => Left(x)}


  def forNodeParser(oldContext : Context) : PackratParser[ForNode] = pushContext(oldContext) >> {context => ("for" ~> ("(" ~> forExpr1(context).? <~ ";") ~ (expressionsParser(context).? <~ ";") ~ (expressionsParser(context).? <~ ")") ~ (nodesParser(context) ^^ {x => Some(x)} | (";" ^^ {x => None})))} ^^
    {
      x =>
        new ForNode(x._1._1._1, x._1._1._2, x._1._2, x._2)
    }
  
}
