package ru.kt15.finomen.compiler.parser.nodes

import scala.util.parsing.combinator.{PackratParsers, JavaTokenParsers}
import ru.kt15.finomen.compiler.parser.types.CTypeParser
import ru.kt15.finomen.compiler.parser.{ContextParser, IdentifierParser}
import ru.kt15.finomen.compiler.ast.nodes._
import ru.kt15.finomen.compiler.parser.expressions.ExpressionsParser
import ru.kt15.finomen.compiler.ast.nodes.FunctionNode
import scala.Some
import ru.kt15.finomen.compiler.ast.types.ArrayType
import ru.kt15.finomen.compiler.env.{Variable, Context}

/**
 * Created by finom_000 on 11.04.2014.
 */
trait FunctionNodeParser  extends AnyRef with JavaTokenParsers with PackratParsers with CTypeParser with IdentifierParser with ExpressionsParser with VariableDeclarationNodeParser with BlockNodeParser with ContextParser {
  def nodesParser(context : Context) : Parser[Node]

  def functionArg(context : Context) : PackratParser[VariableDeclarationNode] = cTypeParser(context) ~ identifierParser ~ rep("[]").? ~ ("=" ~> noCommaExpression(context)).? ^^
    {x =>
      var t = x._1._1._1
      if (x._1._2.isDefined)
        t = x._1._2.get.foldLeft(t) {(a, b) => new ArrayType(a)}
      new VariableDeclarationNode(new Variable(x._1._1._2, t), x._2)
    }

  def arglist(context : Context) : PackratParser[List[VariableDeclarationNode]] = ("(" ^^{x => context}) ~> rep1sep(functionArg(context), ",").? <~ ")" ^^ (_.getOrElse(List[VariableDeclarationNode]()))

  def functionNodeParser(oldContext : Context) : PackratParser[FunctionNode] =
    (((cTypeParser(oldContext) ^^ (Some(_))) | ("void" ^^^ (None))) ~ identifierParser ~ arglist(context)) >> { x => (pushFunction(oldContext, x._1._2, x._1._1, x._2.map(_.variable).toList) ^^ { y => (y, {body : Option[BlockNode] => new FunctionNode(y, x._2, body)}) }) >> {z => ((blockNodeParser(z._1) ^^ (x => z._2(Some(x)))) | (";" ^^^ (z._2(None))))}}

}
