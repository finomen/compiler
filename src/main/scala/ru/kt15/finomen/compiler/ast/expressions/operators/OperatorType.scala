package ru.kt15.finomen.compiler.ast.expressions.operators

/**
 * Created by finom_000 on 27.03.14.
 */
object OperatorType extends Enumeration {
  type OperatorType = Value
  val SuffixIncrement,
      SuffixDecrement,
      FunctionCall,
      ArraySubscript,
      ElementByRef,
      ElementByPointer,
      PrefixIncrement,
      PrefixDecrement,
      UnaryPlus,
      UnaryMinus,
      LogicalNot,
      BitwiseNot,
      Cast,
      Dereference,
      AddressOf,
      SizeOf,
      Multiplication,
      Division,
      Modulo,
      Addition,
      Subtraction,
      BitwiseLeftShift,
      BitwiseRightShift,
      Less,
      LessOrEqual,
      Greater,
      GreaterOrEqual,
      Equal,
      NotEqual,
      BitwiseAnd,
      BitwiseXor,
      BitwiseOr,
      LogicalAnd,
      LogicalOr,
      Ternary,
      Assignment,
      AssignmentSum,
      AssignmentDifference,
      AssignmentProduct,
      AssignmentQuotient,
      AssignmentRemainder,
      AssignmentLeftShift,
      AssignmentRightShift,
      AssignmentBitwiseAnd,
      AssignmentBitwiseXor,
      AssignmentBitwiseOr,
      Comma = Value
}
