package ru.kt15.finomen.compiler.ast.expressions

import scala.text.Document
import scala.text.Document._
import ru.kt15.finomen.compiler.env.Variable
import ru.kt15.finomen.compiler.ast.types.builtin.Char

/**
 * Created by finomen on 07.04.14.
 */
case class VariableAccessExpression(variable : Variable) extends Expression {
  def prettyPrint : Document =
    nest(2, text("- VariableAccessExpression") :/:
      group("variable = " :: text(variable.name))
    )

  def resType = variable.variableType
}
