package ru.kt15.finomen.compiler.ast.expressions

import scala.text.Document
import scala.text.Document._
import ru.kt15.finomen.compiler.ast.types.CType

/**
 * Created by finomen on 15.05.14.
 */
case class StackAccess(offset : Int, t : CType) extends Expression {
  def prettyPrint : Document =
    nest(2, text("- StackAccess") :/:
      group("offset = " :: text(offset.toHexString)) :/:
      group("type = " :: text(t.toString))
    )

  def resType = t
}
