package ru.kt15.finomen.compiler.ast

import ru.kt15.finomen.compiler.ast.nodes._
import ru.kt15.finomen.compiler.ast.expressions._
import scala.text.Document._
import scala.text.Document
import ru.kt15.finomen.compiler.env.Context
import ru.kt15.finomen.compiler.ast.expressions.operators.{MemoryAccess, OperatorType, Operator}
import ru.kt15.finomen.compiler.ast.types.{ConstType, CType}
import ru.kt15.finomen.compiler.ast.types.builtin.Char

/**
 * Created by finomen on 29.05.14.
 */
object Compiler {
  val leave =
    """add esp, 0x40
      |pop ebp
      |pop ebx
      |ret""".stripMargin

  var extern : Set[String] = Set[String]()

  def compile(nodes : List[Node]) : List[String] = {
    nodes.map{node =>
      node match {
        case x : AsciiNode => x.name + ": " + "dw `" + x.value + "`, 0"
        case x : ExternNode => {
          extern += x.name
          "extern " + x.name
        }
        case x : SectionNode => "section ." + x.name
        case x : GlobalIntNode => x.name + ": resd 1"
        case x : LabelNode => x.name + ":"
        case x : FunctionEnter =>
          """push ebx
            |push ebp
            |sub esp, 0x40
            |mov ebp, esp""".stripMargin
        case x : FunctionLeave => leave
        case x : FunctionExitNode => x.expression.map(compileExpression(_) + "\n").getOrElse("") + "add esp, 0x" + x.stack.toHexString + "\n" + leave
        case x : StackModify => if (x.size > 0) "sub esp, 0x" + x.size.toHexString else "add esp, 0x" + (-x.size).toHexString
        case x : JumpNode => "jmp " + x.label
        case x : ConditionalJump => compileExpression(x.expression) +
          """
            |test eax, eax
            |jnz """.stripMargin + x.label
        case x : ExpressionNode => compileExpression(x.expression)
        case _ => defaultPrint(node)
      }
    }
  }

  def regByType(t : CType) : String = {
    t match {
      case x : Char => "al"
      case x : ConstType => regByType(x.innerType)
      case _ => "eax"
    }
  }

  def compileExpression(expr : Expression) : String = {
    expr match {
      case x : Operator => compileOperator(x)
      case x : ScalarExpression => "mov eax, " + x.value
      case x : StackAccess => "xor eax,eax\nmov " + regByType(x.t) + ", [esp + 0x" + x.offset.toHexString + "]"
      case x : SymbolAccess => "mov eax, " + x.name
      case x : MemoryAccess => "mov eax, [" + x.name + "]"
      case x : FunctionAccessExpression => "mov eax, " + (if (extern.contains(x.function)) "[" + x.function + "]" else x.function)
      case _ => "Unsupported expression " + expr.getClass.getCanonicalName
    }
  }

  var exprStack = 0x4

  def pushTemp = {
    exprStack = exprStack + 4
    exprStack.toHexString
  }

  def popTemp = {
    exprStack = exprStack - 4
    (exprStack + 4).toHexString
  }

  def compileOperator(op : Operator) : String = {
    if (op.operator == OperatorType.FunctionCall) {
      var argId = 0
      val args = op.arguments.tail.map{x =>
        argId = argId + 1
        compileExpression(x.left.get) +
        "\nmov [ebp + 0x" + pushTemp + "], eax"
      }

      var pushArgs = List[String]()

      while (argId > 0) {
        pushArgs = pushArgs ++ List(
          "mov eax, [ebp + 0x" + popTemp + "]",
          "push eax"
        )
        argId = argId - 1
      }

      val pop = "add esp, 0x" + (args.length * 4).toHexString
      var res = args ++ pushArgs ++ List(compileExpression(op.arguments.head.left.get), "call eax", pop)
      res.tail.foldLeft(res.head){(l, r) => l + "\n" + r}
    } else {
      if (op.arguments.length == 1) {
          op.operator match {
            case OperatorType.UnaryMinus => compileExpression(op.arguments.head.left.get) + "\nneg eax"
            case OperatorType.UnaryPlus => compileExpression(op.arguments.head.left.get)
            case OperatorType.Dereference => compileExpression(op.arguments.head.left.get) +
              //"\n#" + op.resType.toString +
              "\nmov ecx, eax" +
              "\nxor eax, eax" +
              "\nmov " + regByType(op.resType) + ", [ecx]"
            case OperatorType.LogicalNot => {
              val p = compileExpression(op.arguments.head.left.get)
              uniqLabel = uniqLabel + 1
              p + """
                    |test eax, eax
                    |jz _expr_lbl_""".stripMargin + uniqLabel.toString +
                """
                  |xor eax, eax
                  |jmp _expr_lbl_end_""".stripMargin + uniqLabel.toString +
                  "\n_expr_lbl_".stripMargin + uniqLabel.toString + ":\n" +
                  "inc eax\n" +
                  "_expr_lbl_end_".stripMargin + uniqLabel.toString + ":"
            }
            case OperatorType.BitwiseNot => compileExpression(op.arguments.head.left.get) + "\nnot eax"
            case OperatorType.PrefixIncrement => prefixIncDec("inc", op.arguments.head.left.get)
            case OperatorType.PrefixDecrement => prefixIncDec("dec", op.arguments.head.left.get)
            case OperatorType.SuffixIncrement => suffixIncDec("inc", op.arguments.head.left.get)
            case OperatorType.SuffixDecrement => suffixIncDec("dec", op.arguments.head.left.get)
            case _ => "Unsupported operator " + op.operator
          }
      } else if (op.arguments.length == 2) {
        val res =
          (if (op.operator != OperatorType.Assignment) {
            List(compileExpression(op.arguments.tail.head.left.get),
            "mov [ebp + 0x" + pushTemp + "], eax",
            compileExpression(op.arguments.head.left.get),
            "mov ebx, [ebp + 0x" + popTemp + "]")
          } else {List()}) ++
          List (op.operator match {
            case OperatorType.Assignment => compileExpression(op.arguments(1).left.get) + "\n" + storeValue(op.arguments(0).left.get)
            case OperatorType.Equal => boolBinary("je")
            case OperatorType.Greater => boolBinary("jg")
            case OperatorType.GreaterOrEqual => boolBinary("jge")
            case OperatorType.Less => boolBinary("jl")
            case OperatorType.LessOrEqual => boolBinary("jle")
            case OperatorType.NotEqual => boolBinary("jne")
            case OperatorType.Addition => "add eax, ebx"
            case OperatorType.Subtraction => "sub eax, ebx"
            case OperatorType.Multiplication => "mul ebx"
            case OperatorType.Division => "div ebx"
            case OperatorType.BitwiseLeftShift => "mov ecx, ebx\nshl eax, cl"
            case OperatorType.BitwiseRightShift => "mov ecx, ebx\nshr eax, cl"
            case OperatorType.LogicalOr => {
              uniqLabel = uniqLabel + 1
              """test eax, eax
                |jnz _expr_true_ID
                |test ebx, ebx
                |jnz _expr_true_ID
                |xor eax, eax
                |jmp _expr_end_ID
                |_expr_true_ID:
                |mov eax, 1
                |_expr_end_ID:
              """.stripMargin.replace("ID", uniqLabel.toString)
            }
            case OperatorType.LogicalAnd => {
              uniqLabel = uniqLabel + 1
              """test eax, eax
                |jz _expr_false_ID
                |test ebx, ebx
                |jz _expr_false_ID
                |mov eax, 1
                |jmp _expr_end_ID
                |_expr_false_ID:
                |xor eax, eax
                |_expr_end_ID:
              """.stripMargin.replace("ID", uniqLabel.toString)
            }

            case OperatorType.Modulo =>
              """div eax, ebx
                |mov eax, edx""".stripMargin
            case _ => "Unsupported operator " + op.operator
          })
          res.tail.foldLeft(res.head){(l, r) => l + "\n" + r}
      } else {
        uniqLabel = uniqLabel + 1
        List(
          "test eax, eax",
          "jz _ternary_else_" + uniqLabel.toString,
          compileExpression(op.arguments.tail.head.left.get),
          "jmp _ternary_end_" + uniqLabel.toString,
          "_ternary_else_" + uniqLabel.toString + ":",
          compileExpression(op.arguments.tail.tail.head.left.get),
          "_ternary_end_" + uniqLabel.toString + ":"
        ).foldLeft(compileExpression(op.arguments.head.left.get)){(l, r) => l + "\n" + r}
      }
    }
  }

  var uniqLabel = -1

  def boolBinary(jmp : String) : String = {
    uniqLabel = uniqLabel + 1
    "cmp eax, ebx\n" +
    jmp + " _expr_lbl_" + uniqLabel.toString +
    "\nxor eax, eax" +
    "\njmp _expr_lbl_end_" + uniqLabel.toString +
    "\n_expr_lbl_" + uniqLabel.toString + ":" +
    "\nmov eax, 1" +
    "\n_expr_lbl_end_" + uniqLabel.toString + ":"
  }

  def prefixIncDec(op : String, target : Expression) : String = {
    List(op + " eax",
    storeValue(target)).foldLeft(compileExpression(target)){(l, r) => l + "\n" + r}
  }

  def suffixIncDec(op : String, target : Expression) : String = {
    List(op + " eax",
      storeValue(target),
      (if (op == "inc") "dec" else "inc") + " eax").foldLeft(compileExpression(target)){(l, r) => l + "\n" + r}
  }

  def storeValue(target : Expression) : String = {
    target match {
      case x : StackAccess => "mov [esp + 0x" + x.offset.toHexString + "], eax"
      case x : MemoryAccess => "mov [" + x.name + "], eax"
      case x: Operator => {
        if (x.operator == OperatorType.Dereference) {
          "mov [ebp + 0x" + pushTemp + "], eax\n" +
          compileExpression(x.arguments.head.left.get) +
          "\nmov ebx, eax" +
          "\nmov eax, [ebp + 0x" + popTemp + "]" +
          "\nmov [ebx], " + regByType(x.resType)
        } else {
          "Unsupported store"
        }
      }
      case _ => "Unsupported store"
    }
  }

  def defaultPrint(node : Node) = {
    "Unsupported node " + node.getClass.getCanonicalName
  }
}
