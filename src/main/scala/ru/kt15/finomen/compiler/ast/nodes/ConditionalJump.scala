package ru.kt15.finomen.compiler.ast.nodes

import ru.kt15.finomen.compiler.ast.expressions.Expression
import scala.text.Document
import scala.text.Document._

/**
 * Created by finomen on 29.05.14.
 */
case class ConditionalJump(expression : Expression, label : String) extends Node {
  def prettyPrint : Document =
    nest(2, text("- ConditionalJump") :/:
      group("label = " :: text(label)) :/:
      group("expression = " :: expression.prettyPrint)
    )
}
