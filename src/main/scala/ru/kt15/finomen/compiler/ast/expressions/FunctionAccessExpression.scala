package ru.kt15.finomen.compiler.ast.expressions

import scala.text.Document
import scala.text.Document._
import ru.kt15.finomen.compiler.ast.types.pointers.VoidPointer

/**
 * Created by finom_000 on 11.04.2014.
 */
case class FunctionAccessExpression(function : String) extends Expression{
  def prettyPrint : Document =
    nest(2, text("- FunctionAccessExpression") :/:
      group("function = " :: text(function))
    )

  def resType = new VoidPointer()
}
