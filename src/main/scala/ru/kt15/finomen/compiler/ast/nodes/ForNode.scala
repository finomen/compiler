package ru.kt15.finomen.compiler.ast.nodes

import ru.kt15.finomen.compiler.ast.expressions.Expression
import scala.text.Document
import scala.text.Document._

/**
 * Created by finomen on 07.04.14.
 */
case class ForNode(init : Option[Either[Expression, List[VariableDeclarationNode]]], condition : Option[Expression], step : Option[Expression], body : Option[Node]) extends Node {
  def prettyPrint : Document =
    nest(2, text("- ForNode") :/:
      group("init = " :: init.map(x => x.fold({l => l.prettyPrint}, {r =>
        ((empty:Document) /: r) { (d, f) => d :/: f.prettyPrint }
      })).getOrElse(text("None"))) :/:
      group("condition = " :: condition.map(v => v.prettyPrint).getOrElse(text("None"))) :/:
      group("step = " :: step.map(v => v.prettyPrint).getOrElse(text("None"))) :/:
      group("body = " :: body.map(v => v.prettyPrint).getOrElse(text("None")))
    )
}
