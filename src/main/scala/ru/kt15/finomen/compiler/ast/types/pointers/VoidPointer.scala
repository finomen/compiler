package ru.kt15.finomen.compiler.ast.types.pointers

import ru.kt15.finomen.compiler.ast.types.CType

/**
 * Created by finom_000 on 27.03.14.
 */
class VoidPointer extends CType {
  def size = 4
}
