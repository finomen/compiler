package ru.kt15.finomen.compiler.ast.types.builtin

/**
 * Created by finom_000 on 27.03.14.
 */
class Float extends BuiltInType {
  def size = 4
}
