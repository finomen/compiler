package ru.kt15.finomen.compiler.ast.nodes

import scala.text.Document
import scala.text.Document._
import ru.kt15.finomen.compiler.env.Function

/**
 * Created by finomen on 07.04.14.
 */
case class FunctionNode(function : Function, arguments : List[VariableDeclarationNode], body : Option[BlockNode]) extends Node{
  def prettyPrint : Document =
    nest(2, text("- FunctionNode") :/:
      group("return = " :: function.returnType.map(t => t.prettyPrint).getOrElse(text("void"))) :/:
      group("name = " :: text(function.name)) :/:
      group("arguments = " :/: ((empty:Document) /: arguments) { (d, f) => d :/: f.prettyPrint }) :/:
      group("body = " :: body.map(b => b.prettyPrint).getOrElse(text("None")))
    )
}
