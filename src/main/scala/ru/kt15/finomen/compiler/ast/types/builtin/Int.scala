package ru.kt15.finomen.compiler.ast.types.builtin

import ru.kt15.finomen.compiler.ast.types.builtin.SignModifier.SignModifier
import ru.kt15.finomen.compiler.ast.types.builtin.SizeModifier.SizeModifier

/**
 * Created by finom_000 on 27.03.14.
 */
case class Int(signModifier : SignModifier, sizeModifier : SizeModifier) extends BuiltInType {
  def size : scala.Int = {
    sizeModifier match {
      case SizeModifier.Long => 8
      case SizeModifier.Short => 2
      case SizeModifier.None => 4
    }
  }
}
