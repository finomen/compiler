package ru.kt15.finomen.compiler.ast.nodes

import scala.text.Document
import scala.text.Document._

/**
 * Created by finomen on 29.05.14.
 */
case class ExternNode(name : String) extends Node{
  def prettyPrint : Document =
    nest(2, text("- ExternNode") :/:
      group("name = " :/: text(name))
    )
}
