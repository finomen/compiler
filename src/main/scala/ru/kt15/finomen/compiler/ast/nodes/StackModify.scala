package ru.kt15.finomen.compiler.ast.nodes

import scala.text.Document
import scala.text.Document._

/**
 * Created by finomen on 15.05.14.
 */
case class StackModify(size : Int) extends Node{
  def prettyPrint : Document =
    nest(2, text("- StackModify") :/:
      group("size = " :: text(size.toHexString))
    )
}
