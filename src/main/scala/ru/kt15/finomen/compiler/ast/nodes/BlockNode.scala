package ru.kt15.finomen.compiler.ast.nodes

import scala.text.Document
import scala.text.Document._


/**
 * Created by finomen on 07.04.14.
 */
case class BlockNode(node : List[Node]) extends Node{
  def prettyPrint : Document =
    nest(2, text("- BlockNode") :/:
      group("nodes = " :/: ((empty:Document) /: node) { (d, f) => d :/: f.prettyPrint })
    )
}
