package ru.kt15.finomen.compiler.ast.expressions

import scala.text.Document
import scala.text.Document._


/**
 * Created by finom_000 on 27.03.14.
 */
case class InParens(innerExpression : Expression) extends Expression {
  def prettyPrint : Document =
    nest(2, text("- InParens") :/:
      group("expression = " :: innerExpression.prettyPrint)
    )

  def resType = innerExpression.resType
}
