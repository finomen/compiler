package ru.kt15.finomen.compiler.ast.nodes

import scala.text.Document
import scala.text.Document._

/**
 * Created by finomen on 15.05.14.
 */
case class LabelNode(name : String) extends Node {
  def prettyPrint : Document =
    nest(2, text("- LabelNode") :/:
      group("label = " :: text(name))
    )
}
