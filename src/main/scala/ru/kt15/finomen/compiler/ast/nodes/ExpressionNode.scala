package ru.kt15.finomen.compiler.ast.nodes

import ru.kt15.finomen.compiler.ast.expressions.Expression
import scala.text.Document
import scala.text.Document._

/**
 * Created by finomen on 07.04.14.
 */
case class ExpressionNode(expression : Expression) extends Node {
  def prettyPrint : Document =
    nest(2, text("- ExpressionNode") :/:
      group("expression = " :: expression.prettyPrint)
    )
}
