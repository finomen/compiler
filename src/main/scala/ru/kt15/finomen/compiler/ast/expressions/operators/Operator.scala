package ru.kt15.finomen.compiler.ast.expressions.operators

import ru.kt15.finomen.compiler.ast.types.CType
import ru.kt15.finomen.compiler.ast.expressions.operators.OperatorType.OperatorType
import ru.kt15.finomen.compiler.ast.expressions.Expression
import scala.text._
import Document._
import ru.kt15.finomen.compiler.ast.types.pointers.{PointerType, VoidPointer}

/**
 * Created by finom_000 on 27.03.14.
 */
case class Operator(operator : OperatorType, arguments : List[Either[Expression, CType]]) extends Expression {
  def prettyPrint : Document =
    nest(2, text("- Operator") :/:
      group("operator = " :: text(operator.toString)) :/:
      group("result = " :: resType.prettyPrint) :/:
      group("arguments = " :/: ((empty:Document) /: arguments) { (d, f) => d :/: f.fold({l=>l.prettyPrint}, {r=>r.prettyPrint}) })
    )

  def resType = {
    operator match {
      case OperatorType.Dereference => {
        val t = arguments.head.left.get.resType
        if (t.isInstanceOf[PointerType]) {
          t.asInstanceOf[PointerType].objectType
        } else {
          new VoidPointer()
        }
      }
      case OperatorType.Addition => {
        arguments.head.left.get.resType
      }
      case OperatorType.AddressOf => {
        new PointerType(arguments.head.left.get.resType)
      }
      case _ => new VoidPointer()
    }
  }

}