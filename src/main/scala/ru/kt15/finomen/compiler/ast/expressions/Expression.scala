package ru.kt15.finomen.compiler.ast.expressions

import ru.kt15.finomen.compiler.Printable
import ru.kt15.finomen.compiler.ast.types.CType

/**
 * Created by finom_000 on 27.03.14.
 */
abstract class Expression extends AnyRef with Printable {
  def resType : CType
}
