package ru.kt15.finomen.compiler.ast.types.builtin

import ru.kt15.finomen.compiler.ast.types.builtin.SignModifier.SignModifier

/**
 * Created by finom_000 on 27.03.14.
 */
case class Char(signModifier : SignModifier) extends BuiltInType {
  def size = 1
}
