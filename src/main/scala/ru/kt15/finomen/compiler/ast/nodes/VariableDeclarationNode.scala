package ru.kt15.finomen.compiler.ast.nodes

import ru.kt15.finomen.compiler.ast.expressions.Expression
import ru.kt15.finomen.compiler.ast.types.CType
import scala.text.Document
import scala.text.Document._
import ru.kt15.finomen.compiler.env.Variable

/**
 * Created by finomen on 07.04.14.
 */
case class VariableDeclarationNode(variable : Variable, initialValue : Option[Expression]) extends Node{
  def prettyPrint : Document =
    nest(2, text("- VariableDeclarationNode") :/:
      group("variableName = " :: text(variable.name)) :/:
      group("variablesType = " :: variable.variableType.prettyPrint) :/:
      group("initialValue = " :: initialValue.map(v => v.prettyPrint).getOrElse(text("None")))
    )
}
