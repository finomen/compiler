package ru.kt15.finomen.compiler.ast.nodes

import scala.text.Document
import scala.text.Document._

/**
 * Created by finomen on 16.04.14.
 */
case class PseudoBlockNode(nodes : List[Node]) extends Node {
  def prettyPrint : Document =
    nest(2, text("- PseudoBlockNode") :/:
      group("nodes = " :/: ((empty:Document) /: nodes) { (d, f) => d :/: f.prettyPrint })
    )
}
