package ru.kt15.finomen.compiler.ast.nodes

import ru.kt15.finomen.compiler.ast.expressions.Expression
import scala.text.Document
import scala.text.Document._

/**
 * Created by finomen on 07.04.14.
 */
case class WhileNode(condition : Expression, body : Option[Node]) extends Node{
  def prettyPrint : Document =
    nest(2, text("- WhileNode") :/:
      group("condition = " :: condition.prettyPrint) :/:
      group("body = " :: body.map(v => v.prettyPrint).getOrElse(text("None")))
    )
}
