package ru.kt15.finomen.compiler.ast.types.builtin

/**
 * Created by finom_000 on 27.03.14.
 */
object SignModifier extends Enumeration {
  type SignModifier = Value
  val None, Signed, Unsigned = Value
}
