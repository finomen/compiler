package ru.kt15.finomen.compiler.ast.types

import ru.kt15.finomen.compiler.Printable
import scala.text.Document
import scala.text.Document._

/**
 * Created by finom_000 on 27.03.14.
 */
abstract class CType extends AnyRef with Printable {
  def prettyPrint : Document =
    nest(2, text("- CType") :/:
      group("type = " :: text(toString))
    )

  def size : Int
}
