package ru.kt15.finomen.compiler.ast.types

/**
 * Created by finom_000 on 27.03.14.
 */
case class ConstType(innerType : CType) extends CType {
  def size = innerType.size
}
