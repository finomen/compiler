package ru.kt15.finomen.compiler.ast.expressions

import scala.text.Document
import scala.text.Document._
import ru.kt15.finomen.compiler.ast.types.pointers.{VoidPointer, PointerType}
import ru.kt15.finomen.compiler.ast.types.builtin.{SizeModifier, SignModifier, Char, Int}

/**
 * Created by finomen on 07.04.14.
 */
//FIXME: type safety
case class ScalarExpression(value : Any) extends Expression{
  def prettyPrint : Document =
    nest(2, text("- ScalarExpression") :/:
      group("expression = " :: text(value.toString))
    )

  def resType = {
    value match {
      case x : String => PointerType(Char(SignModifier.Signed))
      case x : Int => Int(SignModifier.Signed, SizeModifier.None)
      case x : Char => Char(SignModifier.Signed)
      case _ => new VoidPointer()
    }
  }

}
