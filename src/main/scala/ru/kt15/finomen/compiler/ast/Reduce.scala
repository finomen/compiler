package ru.kt15.finomen.compiler.ast

import ru.kt15.finomen.compiler.ast.nodes._
import ru.kt15.finomen.compiler.ast.expressions._
import ru.kt15.finomen.compiler.ast.types.{CType, AliasType}
import ru.kt15.finomen.compiler.ast.nodes.WhileNode
import ru.kt15.finomen.compiler.ast.nodes.TypedefNode
import ru.kt15.finomen.compiler.ast.nodes.ReturnNode
import ru.kt15.finomen.compiler.env.Variable
import ru.kt15.finomen.compiler.ast.nodes.BlockNode
import ru.kt15.finomen.compiler.ast.nodes.FunctionNode
import ru.kt15.finomen.compiler.ast.expressions.CallExpression
import ru.kt15.finomen.compiler.ast.expressions.InParens
import ru.kt15.finomen.compiler.ast.nodes.DoWhileNode
import ru.kt15.finomen.compiler.ast.expressions.operators._
import ru.kt15.finomen.compiler.ast.nodes.LabelNode
import ru.kt15.finomen.compiler.ast.nodes.ForNode
import ru.kt15.finomen.compiler.ast.nodes.PseudoBlockNode
import ru.kt15.finomen.compiler.ast.nodes.ExpressionNode
import ru.kt15.finomen.compiler.ast.nodes.VariableDeclarationNode
import ru.kt15.finomen.compiler.ast.nodes.IfNode
import ru.kt15.finomen.compiler.ast.types.CType
import sun.org.mozilla.javascript.internal.ast.Assignment
import sun.org.mozilla.javascript.internal.IdFunctionCall
import ru.kt15.finomen.compiler.ast.nodes.WhileNode
import ru.kt15.finomen.compiler.ast.expressions.InParens
import ru.kt15.finomen.compiler.ast.nodes.PseudoBlockNode
import scala.Some
import ru.kt15.finomen.compiler.ast.nodes.IfNode
import ru.kt15.finomen.compiler.ast.nodes.StackModify
import ru.kt15.finomen.compiler.ast.nodes.ExternNode
import ru.kt15.finomen.compiler.env.Variable
import ru.kt15.finomen.compiler.ast.nodes.BlockNode
import ru.kt15.finomen.compiler.ast.expressions.StackAccess
import ru.kt15.finomen.compiler.ast.expressions.VariableAccessExpression
import ru.kt15.finomen.compiler.ast.expressions.FunctionAccessExpression
import ru.kt15.finomen.compiler.ast.nodes.TypedefNode
import ru.kt15.finomen.compiler.ast.nodes.GlobalIntNode
import ru.kt15.finomen.compiler.ast.nodes.ConditionalJump
import ru.kt15.finomen.compiler.ast.expressions.CallExpression
import ru.kt15.finomen.compiler.ast.expressions.operators.Operator
import ru.kt15.finomen.compiler.ast.nodes.LabelNode
import ru.kt15.finomen.compiler.ast.nodes.ForNode
import ru.kt15.finomen.compiler.ast.nodes.FunctionExitNode
import ru.kt15.finomen.compiler.ast.nodes.ExpressionNode
import ru.kt15.finomen.compiler.ast.nodes.AsciiNode
import ru.kt15.finomen.compiler.ast.types.AliasType
import ru.kt15.finomen.compiler.ast.types.builtin.Char
import ru.kt15.finomen.compiler.ast.nodes.SectionNode
import ru.kt15.finomen.compiler.ast.expressions.operators.MemoryAccess
import ru.kt15.finomen.compiler.ast.nodes.ReturnNode
import ru.kt15.finomen.compiler.ast.nodes.FunctionNode
import ru.kt15.finomen.compiler.ast.nodes.DoWhileNode
import ru.kt15.finomen.compiler.ast.expressions.ScalarExpression
import ru.kt15.finomen.compiler.ast.nodes.VariableDeclarationNode
import ru.kt15.finomen.compiler.ast.expressions.SymbolAccess
import ru.kt15.finomen.compiler.ast.nodes.JumpNode
import ru.kt15.finomen.compiler.ast.types.pointers.PointerType

/**
 * Created by finom_000 on 18.04.2014.
 */
object Reduce {
  var loopIdentifier : Int = 0

  def reduceFile(rootList: List[Node]): List[Node] = {
    val rootOld = rootList.map{node =>
      node match {
        case x : PseudoBlockNode => x.nodes
        case _ => List(node)
      }
    }.flatten

    val data = rootOld.filter(_.isInstanceOf[VariableDeclarationNode]).map{x =>  new GlobalIntNode(x.asInstanceOf[VariableDeclarationNode].variable.name, 0)}

    val extern = rootOld.filter(_.isInstanceOf[FunctionNode]).map(_.asInstanceOf[FunctionNode]).filter(_.body == None).map{x => new ExternNode(x.function.name)} ++ List(new ExternNode("ExitProcess"))

    val glob = rootOld.filter(_.isInstanceOf[VariableDeclarationNode]).map{x =>  x.asInstanceOf[VariableDeclarationNode].variable.name}

    val startBody = rootOld.filter(_.isInstanceOf[VariableDeclarationNode]).map{x =>
      x.asInstanceOf[VariableDeclarationNode].initialValue.map{y =>
        new ExpressionNode(new Operator(OperatorType.Assignment,
                        List(Left(new VariableAccessExpression(x.asInstanceOf[VariableDeclarationNode].variable)),
                             Left(y))))}}.flatten ++ List(
        new ExpressionNode(new Operator(OperatorType.FunctionCall, List(Left(new FunctionAccessExpression("main"))))),
      new ExpressionNode(new Operator(OperatorType.FunctionCall, List(Left(new FunctionAccessExpression("ExitProcess")), Left(new ScalarExpression(0)))))
    )

    val root = replaceStringWithGlob(replaceVarWithGlob(List(new FunctionNode(new ru.kt15.finomen.compiler.env.Function("start", None, List()), List(), Some(new BlockNode(startBody)))) ++ rootOld, glob))

    val source = root.filter{x => !x.isInstanceOf[VariableDeclarationNode]}
    val text = source.map(reduceTree).flatten

    var sid = 0

    val rodata = strings.map{s =>
      sid = sid + 1
      new AsciiNode("_string_" + (sid - 1).toString, s)
    }

    extern ++ List(new SectionNode("rodata")) ++ rodata ++ List(new SectionNode("data")) ++ data ++ List(new SectionNode("code")) ++ text
  }

  def reduceTree(root: Node): List[Node] = {
    root match {
      case x: FunctionNode => {
        var off = 4
        x.body.map {
          b => List(
            new LabelNode(x.function.name),
            new FunctionEnter) ++
            replaceVarWithStack(reduceTree(b).map {
              n => pushStack(n, 4)
            }, x.arguments.map {
              a =>
                val res = (a.variable, off + 0x40 + 0x8 + 0x4)
                off += 4
                res
            }) ++
            List(new FunctionLeave)
        }.getOrElse(List())
      }
      case x: BlockNode => {
        val vars = extractVars(x.node)
        val size = vars.length * 4
        var off = 0
        val stacks = vars.map{ v =>
          val res = (v.variable, off)
          off += 4
          res
        }
        List(StackModify(size)) ++ replaceVarWithStack(x.node.map{n => reduceTree(freeStack(n, size))}.flatten.map{n =>
          pushStack(n, size)
        }, stacks) ++ List(StackModify(-size))
      }
      case x: PseudoBlockNode => x.nodes.map(reduceTree(_)).flatten.toList
      case x: TypedefNode => List[Node]()
      case x: ExpressionNode => List[Node](new ExpressionNode(reduceExpression(x.expression)))
      case x: ReturnNode => List[Node](new ReturnNode(x.expression.map(reduceExpression(_))))
      case x: VariableDeclarationNode => x.initialValue.map {
        e => List(new ExpressionNode(new Operator(OperatorType.Assignment, List(Left(new VariableAccessExpression(x.variable)), Left(e)))))
      }.getOrElse(List[Node]())

      case x : WhileNode => reduceWhile(x).map(reduceTree).flatten
      case x : DoWhileNode => reduceDoWhile(x).map(reduceTree).flatten
      case x : ForNode => reduceTree(reduceFor(x))
      case x : IfNode => reduceIf(x).map(reduceTree).flatten
      case _ => List[Node](root)
    }
  }

  def freeStack(node : Node, size : Int) : Node = {
    val r = transformNodes(node, n => n.isInstanceOf[ReturnNode], {n =>
      FunctionExitNode(n.asInstanceOf[ReturnNode].expression, 0)
    })

    transformNodes(r, n => n.isInstanceOf[FunctionExitNode], {n =>
      FunctionExitNode(n.asInstanceOf[FunctionExitNode].expression, n.asInstanceOf[FunctionExitNode].stack + size)
    })
  }

  def reduceWhile(node : WhileNode) : List[Node] = {
    val label = "while_loop_" + (loopIdentifier).toString
    val labelEnd = "while_loop_end_" + (loopIdentifier).toString
    loopIdentifier = loopIdentifier + 1

    var res : List[Node] = List(
      new LabelNode(label),
      new ConditionalJump(new Operator(OperatorType.LogicalNot, List(Left(reduceExpression(node.condition)))), labelEnd))
    res = res ++ node.body.map(List(_)).getOrElse(List[Node]())
    res = res ++ List(new JumpNode(label), new LabelNode(labelEnd))
    res
  }

  def reduceDoWhile(node : DoWhileNode) : List[Node] = {
    val label = "do_while_loop_" + (loopIdentifier).toString
    loopIdentifier = loopIdentifier + 1

    List(
      new LabelNode(label),
      node.body,
      new ConditionalJump(reduceExpression(node.condition), label))
  }

  def reduceFor(node : ForNode) : BlockNode = {
    val whileBody = List(
        node.body.map(List(_)).getOrElse(List()),
        node.step.map{x => List(new ExpressionNode(x))}.getOrElse(List())
    ).flatten

    new BlockNode(List(
      node.init.map{x =>
        x.fold(l => List(new ExpressionNode(reduceExpression(l))), r => r)
      }.getOrElse(List())
    ).flatten ++ List(new WhileNode(node.condition.getOrElse(ScalarExpression(1)), Some(new PseudoBlockNode(whileBody)))))
  }

  def reduceIf(node : IfNode) : List[Node] = {
    val labelElse = "if_else" + (loopIdentifier).toString
    val labelEnd = "if_end" + (loopIdentifier).toString
    loopIdentifier = loopIdentifier + 1

    List(
      new ConditionalJump(new Operator(OperatorType.LogicalNot, List(Left(node.condition))), labelElse),
      node.trueNode,
      new JumpNode(labelEnd),
      new LabelNode(labelElse),
      node.elseNode.getOrElse(new PseudoBlockNode(List())),
      new LabelNode(labelEnd)
    )
  }


  def extractVars(nodes : List[Node]) : List[VariableDeclarationNode] = {
    nodes.map(n =>
      n match {
        case x : VariableDeclarationNode => List(x)
        case x : PseudoBlockNode => extractVars(x.nodes)
        case _ => List()
      }
    ).flatten
  }

  def replaceVarWithStack(node: List[Node], v: List[(Variable, Int)]): List[Node] = {
    node.map {
      n =>
        v.foldLeft(n) {
          (n, v) =>
            replaceVarWithStack(n, v._1, v._2)
        }
    }
  }

  def replaceVarWithStack(node: Node, v: Variable, offset: Int): Node = {
    transformExpressions(node, (e: Expression) =>
      if (e.isInstanceOf[VariableAccessExpression])
        e.asInstanceOf[VariableAccessExpression].variable.name == v.name
      else
        false
      , (sa: Expression) => StackAccess(offset, v.variableType))
  }

  var strings = List[String]()

  def replaceVarWithGlob(node: List[Node], v: List[String]): List[Node] = {
    node.map {
      n =>
        v.foldLeft(n) {
          (n, v) =>
            replaceVarWithGlob(n, v)
        }
    }
  }

  def replaceVarWithGlob(node: Node, name: String): Node = {
    transformExpressions(node, (e: Expression) =>
      if (e.isInstanceOf[VariableAccessExpression])
        e.asInstanceOf[VariableAccessExpression].variable.name == name
      else
        false
      , (sa: Expression) => MemoryAccess(name))
  }

  def replaceStringWithGlob(node: List[Node]): List[Node] = {
    node.map {
      n =>
        replaceStringWithGlob(n)
    }
  }

  def replaceStringWithGlob(node: Node): Node = {
    transformExpressions(node, (e: Expression) =>
      if (e.isInstanceOf[ScalarExpression])
        e.asInstanceOf[ScalarExpression].value.isInstanceOf[String]
      else
        false
      , (sa: Expression) => {
        strings = strings ++ List(sa.asInstanceOf[ScalarExpression].value.asInstanceOf[String])
        SymbolAccess("_string_" + (strings.length - 1).toString)
      })
  }

  def pushStack(node: Node, size: Int): Node = {
    transformExpressions(node, (e: Expression) => e.isInstanceOf[StackAccess], (sa: Expression) => StackAccess(sa.asInstanceOf[StackAccess].offset + size, sa.asInstanceOf[StackAccess].t))
  }

  def transformExpressions(node: Node, selector: Expression => Boolean, transformer: Expression => Expression): Node = {
    transformNodes(node, n =>
      n.isInstanceOf[ExpressionNode] ||
        n.isInstanceOf[VariableDeclarationNode] ||
        n.isInstanceOf[ReturnNode] ||
        n.isInstanceOf[FunctionExitNode] ||
        n.isInstanceOf[ForNode] ||
        n.isInstanceOf[WhileNode] ||
        n.isInstanceOf[DoWhileNode] ||
        n.isInstanceOf[IfNode] ||
        n.isInstanceOf[ConditionalJump]
      ,
      n => {
        val foo = (e: Expression) => transformExpressions(e, selector, transformer)
        n match {
          case x: ExpressionNode => ExpressionNode(foo(x.expression))
          case x: ReturnNode => ReturnNode(x.expression.map(foo))
          case x: FunctionExitNode => FunctionExitNode(x.expression.map(foo), x.stack)
          case x: ForNode => ForNode(x.init.map {
            i => i.fold(l => Left(foo(l)), r => Right(r))
          }, x.condition.map(foo), x.step.map(foo), x.body)
          case x: WhileNode => WhileNode(foo(x.condition), x.body)
          case x: DoWhileNode => DoWhileNode(foo(x.condition), x.body)
          case x: IfNode => IfNode(foo(x.condition), x.trueNode, x.elseNode)
          case x: ConditionalJump => ConditionalJump(foo(x.expression), x.label)
          case x: VariableDeclarationNode => VariableDeclarationNode(x.variable, x.initialValue.map(foo))
        }
      })
  }

  def transformExpressions(expr: Expression, selector: Expression => Boolean, transformer: Expression => Expression): Expression = {
    if (selector(expr))
      transformer(expr)
    else {
      val foo = (e: Expression) => transformExpressions(e, selector, transformer)
      expr match {
        case x: Operator => Operator(x.operator, x.arguments.map {
          x => x.fold(a => Left(foo(a)), Right(_))
        })
        case x: CallExpression => CallExpression(x.function, x.arguments.map(foo))
        case x: InParens => foo(x.innerExpression)
        case _ => expr
      }
    }
  }

  def transformNodes(inputNode: Node, selector: Node => Boolean, transformer: Node => Node): Node = {
    val node = if (selector(inputNode)) {
      transformer(inputNode)
    } else {
      inputNode
    }

    val foo = (n: Node) => transformNodes(n, selector, transformer)
    node match {
      case x: BlockNode => BlockNode(x.node.map(foo))
      case x: PseudoBlockNode => PseudoBlockNode(x.nodes.map(foo))
      case x: IfNode => IfNode(x.condition, foo(x.trueNode), x.elseNode.map(foo))
      case x: WhileNode => WhileNode(x.condition, x.body.map(foo))
      case x: DoWhileNode => DoWhileNode(x.condition, foo(x.body))
      case x: ForNode => ForNode(x.init.map {
        i => i.fold(l => Left(l), r => Right(r.map(n => foo(n).asInstanceOf[VariableDeclarationNode])))
      }, x.condition, x.step, x.body.map(foo))
      case x: FunctionNode => FunctionNode(x.function, x.arguments, x.body.map {
        x => foo(x).asInstanceOf[BlockNode]
      })
      case _ => node
    }
  }

  def reduceExpression(expr: Expression): Expression = {
    expr match {
      case x: InParens => reduceExpression(x.innerExpression)
      case x: Operator => {
        x.operator match {
          case OperatorType.ArraySubscript => {
            var size = 4
            if (x.arguments(0).left.get.isInstanceOf[VariableAccessExpression]) {
              val vt = x.arguments(0).left.get.asInstanceOf[VariableAccessExpression].variable.variableType
              if (vt.isInstanceOf[PointerType]) {
                size = vt.asInstanceOf[PointerType].objectType.size
              }
            }

            Operator(OperatorType.Dereference, List(Left(reduceExpression(
              Operator(
                OperatorType.Addition,
                List(x.arguments(0),
                  Left(Operator(
                    OperatorType.Multiplication,
                    List(x.arguments(1), Left(ScalarExpression(size)))
                  ))
                )
              )))))
          }
          case _ => Operator(x.operator, x.arguments.map(_.fold({l => Left(reduceExpression(l))}, {r => Right(r)})))
        }
      }
      case _ => expr
    }
  }

  def reduceType(t: CType): CType = {
    t match {
      case x: AliasType => x.innerType
      case _ => t
    }
  }

}
