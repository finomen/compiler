package ru.kt15.finomen.compiler.ast.nodes

import ru.kt15.finomen.compiler.ast.expressions.Expression
import scala.text.Document
import scala.text.Document._

/**
 * Created by finomen on 29.05.14.
 */
case class FunctionExitNode(expression : Option[Expression], stack : Int) extends Node {
  def prettyPrint : Document =
    nest(2, text("- FunctionExitNode") :/:
      group("expression = " :: expression.map{x => x.prettyPrint}.getOrElse(text("None"))) :/:
      group("stack = " :: text(stack.toString))
    )
}
