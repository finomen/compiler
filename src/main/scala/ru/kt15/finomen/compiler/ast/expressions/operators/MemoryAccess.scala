package ru.kt15.finomen.compiler.ast.expressions.operators

import ru.kt15.finomen.compiler.ast.expressions.Expression
import scala.text.Document
import scala.text.Document._
import ru.kt15.finomen.compiler.ast.types.pointers.VoidPointer

/**
 * Created by finomen on 29.05.14.
 */
case class MemoryAccess(name : String) extends Expression {
  def prettyPrint : Document =
    nest(2, text("- MemoryAccess") :/:
      group("label = " :: text(name))
    )

  def resType = new VoidPointer()
}
