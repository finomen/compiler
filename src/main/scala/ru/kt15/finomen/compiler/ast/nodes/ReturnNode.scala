package ru.kt15.finomen.compiler.ast.nodes

import ru.kt15.finomen.compiler.ast.expressions.Expression
import scala.text.Document
import scala.text.Document._

/**
 * Created by finom_000 on 18.04.2014.
 */
case class ReturnNode(expression : Option[Expression]) extends Node {
  def prettyPrint : Document =
    nest(2, text("- ReturnNode") :/:
      group("expression = " :: expression.map{x => x.prettyPrint}.getOrElse(text("None")))
    )
}