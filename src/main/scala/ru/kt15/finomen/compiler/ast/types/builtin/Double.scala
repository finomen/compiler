package ru.kt15.finomen.compiler.ast.types.builtin

import ru.kt15.finomen.compiler.ast.types.builtin.SizeModifier.SizeModifier

/**
 * Created by finom_000 on 27.03.14.
 */
case class Double(sizeModifier : SizeModifier) extends BuiltInType {
  def size = 8
}
