package ru.kt15.finomen.compiler.ast.types

/**
 * Created by finom_000 on 27.03.14.
 */
case class AliasType(innerType : CType) extends CType {
  def flatten : CType = innerType match {
    case t:AliasType => t.flatten
    case _ => innerType
  }

  def size = innerType.size
}
