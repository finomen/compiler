package ru.kt15.finomen.compiler.ast.nodes

import ru.kt15.finomen.compiler.ast.types.CType
import scala.text.Document
import scala.text.Document._

/**
 * Created by finomen on 07.04.14.
 */
case class TypedefNode(innerType : CType, name : String) extends Node {
  def prettyPrint : Document =
    nest(2, text("- TypedefNode") :/:
      group("name = " :: text(name)) :/:
      group("type = " :: innerType.prettyPrint)
    )
}
