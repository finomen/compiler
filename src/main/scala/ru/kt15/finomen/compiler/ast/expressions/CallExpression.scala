package ru.kt15.finomen.compiler.ast.expressions

import scala.text.Document
import scala.text.Document._
import ru.kt15.finomen.compiler.ast.types.pointers.{VoidPointer, PointerType}


/**
 * Created by finomen on 07.04.14.
 */
case class CallExpression(function : String, arguments : List[Expression]) extends Expression {
  def prettyPrint : Document =
    nest(2, text("- CallExpression") :/:
      group("function = " :: text(function)) :/:
      group("arguments = " :/: ((empty:Document) /: arguments) { (d, f) => d :/: f.prettyPrint })
    )

  def resType = new VoidPointer()
}
