package ru.kt15.finomen.compiler.ast.types.builtin

/**
 * Created by finom_000 on 27.03.14.
 */
object SizeModifier extends Enumeration {
  type SizeModifier = Value
  val None, Long, Short = Value
}
