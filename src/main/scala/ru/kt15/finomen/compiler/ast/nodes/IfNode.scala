package ru.kt15.finomen.compiler.ast.nodes

import ru.kt15.finomen.compiler.ast.expressions.Expression
import scala.text.Document
import scala.text.Document._

/**
 * Created by finomen on 07.04.14.
 */
case class IfNode(condition : Expression, trueNode : Node, elseNode : Option[Node]) extends Node{
  def prettyPrint : Document =
    nest(2, text("- IfNode") :/:
      group("condition = " :: condition.prettyPrint) :/:
      group("trueNode = " :: trueNode.prettyPrint) :/:
      group("elseNode = " :: elseNode.map(v => v.prettyPrint).getOrElse(text("None")))
    )
}
