package ru.kt15.finomen.compiler.env

import ru.kt15.finomen.compiler.ast.types.CType
import ru.kt15.finomen.compiler.ast.nodes.{VariableDeclarationNode, FunctionNode}

/**
 * Created by finomen on 02.04.14.
 */
class Context {
  private var typedefs_ : Map[String, CType] = Map[String, CType]()
  private var variables_ : Map[String, Variable] = Map[String, Variable]()
  private var functions_ : Map[String, Function] = Map[String, Function]()

  def typedefs : Map[String, CType] = parent.map(_.typedefs).getOrElse(Map[String, CType]()) ++ typedefs_
  def variables : Map[String, Variable] = parent.map(_.variables).getOrElse(Map[String, Variable]()) ++ variables_
  def functions : Map[String, Function] = parent.map(_.functions).getOrElse(Map[String, Function]()) ++ functions_

  def +=[T] (arg : T) = arg match {
    case x : (String, CType) => typedefs_ += x
    case x : Variable => variables_ += (x.name -> x)
    case x : Function => functions_ += (x.name -> x)
  }

  var parent : Option[Context] = None

  def push : Context = {
    val newContext = new Context()
    newContext.parent = Some(this)
    newContext
  }

  def pushFunction(name : String, returnType : Option[CType], arguments : List[Variable]) : Function = {
    val newContext = new Function(name, returnType, arguments)
    arguments.foreach(newContext += _)
    this += newContext
    newContext.parent = Some(this)
    newContext
  }
}