package ru.kt15.finomen.compiler.env

import ru.kt15.finomen.compiler.ast.types.CType

/**
 * Created by finom_000 on 19.04.2014.
 */
case class Function(name : String, returnType : Option[CType], arguments : List[Variable]) extends Context {

}
