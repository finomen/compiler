package ru.kt15.finomen.compiler

import ru.kt15.finomen.compiler.parser.nodes.NodesParser
import ru.kt15.finomen.compiler.ast.Reduce
import scala.text.Document._
import scala.text.Document
import ru.kt15.finomen.compiler.env.Context
import java.io.FileWriter

/**
 * Created by finomen on 18.04.14.
 */
object Parser extends AnyRef with NodesParser{
  val source =
    """
      |//Comment 1
      |void printf(); /*
      |Multiline comment
      |*/
      |void main(int argc, const char * argv[]) {
      |   for (int i = 0; i < argc; ++i) {
      |     const char * arg = argv[i];
      |     printf(arg);
      |   }
      |
      |   --argc;
      |
      |}
    """.stripMargin

  val source1 =
    """
      |int a = 3 + 5 + 7, b;
      |int c = a += b += 7;
      |
      |int b1 = 2 + 2 * 2, b2 = (2 + 2) * 2;
      |
      |void f(int x, int y, int z, int q = 1, int x = 7 * 6 * 9 << 4) { // some comment
      |    int zzz = x + y;
      |    ++y;
      |    return;
      |}
      |
      |/*
      |
      |Small story
      |небольшой блок текста
      |
      |*/
      |void printf();
      |int main(int argc, const char * argv[]) {
      |    while (c == 2) {
      |        printf(45, 54, 0, 12 * 12, c);
      |    }
      |
      |    do {
      |        printf("%s", 25);  // aba caba aba
      |    } while (c == 2);
      |
      |    //for (int i = 0; i < 15; ++i) {}
      |
      |    int i;
      |    for (; 7 == 9; i++);
      |
      |    int d;
      |    if (a == 3)
      |        return 2;
      |    else {
      |        return 9;
      |    }
      |
      |     2.2 + 3.2f;
      |    int x;
      |    int y;
      |
      |    x >> a >> b >> c >> d;
      |    y << a + b << b << c << (b << c);
      |}
      |
      |int abc = ++ ++ a;
    """.stripMargin

  def using[A <: {def close(): Unit}, B](param: A)(f: A => B): B =
    try { f(param) } finally { param.close() }

  def writeToFile(fileName:String, data:String) =
    using (new FileWriter(fileName)) {
    fileWriter => fileWriter.write(data)
  }

  def main(args: Array[String]) = {
      val sourceCode =
        if (args.isEmpty)
          source1
        else
          io.Source.fromFile(args(0)).mkString

      val res = parseAll(sourceParser, sourceCode)
      Console.println(
        if (!res.isInstanceOf[NoSuccess])
        {
          val writer = new java.io.StringWriter
          val writer1 = new java.io.StringWriter
          val src = Reduce.reduceFile(res.get.nodes)
          val asm = ast.Compiler.compile(src)
          ((empty:Document) /: res.get.nodes) { (d, f) => d :/: f.prettyPrint }.format(1, writer1)
          ((empty:Document) /: src) { (d, f) => d :/: f.prettyPrint }.format(1, writer)

          var asmCode = "global start\n" + asm.foldLeft(""){(l, r) => l + "\n" + r}
          if (args.isEmpty)
            res
          else {
            writeToFile(args(0).replace(".c", ".ast"), writer1.toString)
            writeToFile(args(0).replace(".c", ".source"), writer.toString)
            writeToFile(args(0).replace(".c", ".asm"), asmCode)
          }
        }
        else
          res
      )
  }
}
