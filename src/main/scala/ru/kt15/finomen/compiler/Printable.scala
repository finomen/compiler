package ru.kt15.finomen.compiler

import scala.text.Document

/**
 * Created by finomen on 18.04.14.
 */
trait Printable {
  def prettyPrint : Document
}
